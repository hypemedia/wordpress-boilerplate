<?php
	/**
	 * The main template file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$show_pagination = hm_master_show_bootstrap_pagination();
	
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = 'none';
	
	get_header();
?>

<main id="main-content" class="search-template">
	<?php if ( get_search_query() ): ?>
        <?php if ( have_posts() ): ?>
            <section class="articles py-4">
                <div class="section-inner container">
                    <div class="row">
                        <div class="col-12">
                            <?php while ( have_posts() ): the_post();
                                get_template_part( '/template-parts/search/content' );
                            endwhile; ?>
                        </div>
                    </div>
                </div>
            </section>
            <?php if ( $show_pagination ): ?>
                <section class="pagination pb-4">
                    <div class="section-inner container">
                        <?php get_template_part( '/template-parts/partials/pagination' ); ?>
                    </div>
                </section>
		    <?php endif; ?>
        <?php else: ?>
            <section id="articles" class="articles home py-4">
                <div class="section-inner container">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <?php get_template_part( '/template-parts/not-found/search' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
	<?php else: ?>
        <section id="articles" class="articles home py-4">
            <div class="section-inner container">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-8 col-md-6 mx-auto">
                                <div class="textbox">
			                        <?php get_template_part('template-parts/forms/search-form'); ?>
                                    <p class="text-center mb-1"><?php printf( __( 'Suchen Sie hier nach Informationen, Produkten und News.', 'hm_master' ), get_search_query() ); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
</main>

<?php get_footer(); ?>
