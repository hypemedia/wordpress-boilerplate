<?php
	/**
	 * Custom Post Type for predefined
	 * content
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	/**
	 * Create Post Type for the Custom Grid-Cards
	 */
	function hm_master_create_cards_type() {
		register_post_type( 'defined_content',
			array(
				'labels'            => array(
					'name'               => 'Globale Widgets',
					'singular_name'      => 'Globales Widget',
					'add_new'            => 'Erstellen',
				),
				'public'            => false,
				'show_ui'			=> true,
				'menu_position'     => 60,
				'supports'          => array( 'title', 'revisions', 'custom-fields' ),
			)
		);
	}
	add_action( 'init', 'hm_master_create_cards_type' );
