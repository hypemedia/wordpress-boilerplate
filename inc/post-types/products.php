<?php
	/**
	 * Custom Post Type for Products
	 * + related functions
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	
	/**
	 * Register Products post type
	 */
	function hm_master_products_post_type() {
		
		$labels  = array(
			'name'                  => _x( 'Produkte', 'Allgemeiner Name', 'hm_master' ),
			'singular_name'         => _x( 'Produkt', 'Singular Name', 'hm_master' ),
			'menu_name'             => __( 'Produkte', 'hm_master' ),
			'name_admin_bar'        => __( 'Produkte', 'hm_master' ),
			'archives'              => __( 'Produktarchiv', 'hm_master' ),
			'attributes'            => __( 'Produkte-Attribute', 'hm_master' ),
			'all_items'             => __( 'Alle Produkte', 'hm_master' ),
			'add_new_item'          => __( 'Produkt hinzufügen', 'hm_master' ),
			'add_new'               => __( 'Neues Produkt', 'hm_master' ),
			'new_item'              => __( 'Neu', 'hm_master' ),
			'edit_item'             => __( 'Bearbeiten', 'hm_master' ),
			'update_item'           => __( 'Aktualiseren', 'hm_master' ),
			'view_item'             => __( 'Produkt ansehen', 'hm_master' ),
			'view_items'            => __( 'Produkte ansehen', 'hm_master' ),
			'search_items'          => __( 'Suchen', 'hm_master' ),
			'not_found'             => __( 'Nicht gefunden', 'hm_master' ),
			'not_found_in_trash'    => __( 'Nicht gefunden', 'hm_master' ),
			'featured_image'        => __( 'Produktbild', 'hm_master' ),
			'set_featured_image'    => __( 'Produktbild festlegen', 'hm_master' ),
			'remove_featured_image' => __( 'Produktbild entfernen', 'hm_master' ),
			'use_featured_image'    => __( 'Produktbild verwenden', 'hm_master' ),
			'insert_into_item'      => __( 'Einfügen', 'hm_master' ),
			'uploaded_to_this_item' => __( 'Hochladen', 'hm_master' ),
		);
		$rewrite = array(
			'slug'       => 'produkte',
			'with_front' => true,
			'pages'      => true,
			'feeds'      => true,
		);
		$args    = array(
			'label'               => __( 'Produkt', 'hm_master' ),
			'description'         => __( 'Produkte', 'hm_master' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', 'excerpt' ),
			'taxonomies'          => array( 'indication', ' technology' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-products',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
			'show_in_rest'        => true,
		);
		register_post_type( 'hm_master_products', $args );
		
	}
	
	add_action( 'init', 'hm_master_products_post_type', 0 );
