<?php
	/**
	 * Removes WordPress-Comment functions
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @since 1.2.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	/**
	 * Disable Comments on Media
	 *
	 * @param $open
	 * @param $post_id
	 *
	 * @return bool
	 */
	function hm_master_filter_media_comment_status( $open, $post_id ) {
		$post = get_post( $post_id );
		if ( $post->post_type == 'attachment' ) {
			return false;
		}
		
		return $open;
	}
	
	add_filter( 'comments_open', 'hm_master_filter_media_comment_status', 10, 2 );
	
	/**
	 * Removes from admin menu
	 */
	function hm_master_remove_admin_menus() {
		remove_menu_page( 'edit-comments.php' );
	}
	
	add_action( 'admin_menu', 'hm_master_remove_admin_menus' );
	
	/**
	 * Removes comment function
	 */
	function hm_master_remove_comment_support() {
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	}
	
	add_action( 'init', 'hm_master_remove_comment_support', 100 );
	
	/**
	 * Removes from admin bar
	 */
	function hm_master_admin_bar_render() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu( 'comments' );
	}
	
	add_action( 'wp_before_admin_bar_render', 'hm_master_admin_bar_render' );
