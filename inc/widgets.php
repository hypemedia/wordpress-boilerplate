<?php
	/**
	 * Theme Widgets registration
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @version 1.0.1
	 * @since 1.2.0
	 * @author hype.media <web@hype-media.de>
	 */

	function hm_master_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Footer Spalte 1', 'hm_master' ),
			'id'            => 'footer-column-1',
			'description'   => esc_html__( 'Fügen Sie hier Widgets hinzu, um Inhalte oben im Fußzeilenbereich anzuzeigen.', 'hm_master' ),
			'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Spalte 2', 'hm_master' ),
			'id'            => 'footer-column-2',
			'description'   => esc_html__( 'Fügen Sie hier Widgets hinzu, um Inhalte oben im Fußzeilenbereich anzuzeigen.', 'hm_master' ),
			'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Spalte 3', 'hm_master' ),
			'id'            => 'footer-column-3',
			'description'   => esc_html__( 'Fügen Sie hier Widgets hinzu, um Inhalte oben im Fußzeilenbereich anzuzeigen.', 'hm_master' ),
			'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Spalte 4', 'hm_master' ),
			'id'            => 'footer-column-4',
			'description'   => esc_html__( 'Fügen Sie hier Widgets hinzu, um Inhalte oben im Fußzeilenbereich anzuzeigen.', 'hm_master' ),
			'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="title">',
			'after_title'   => '</h3>',
		) );

//		register_sidebar( array(
//			'name'          => esc_html__( 'Mobile Menu', 'hm_master' ),
//			'id'            => 'mobile-menu-1',
//			'description'   => esc_html__( 'Fügen Sie hier Widgets hinzu, um Inhalte unten im mobilen Hauptmenü anzuzeigen.', 'hm_master' ),
//			'before_widget' => '<div id="%1$s" class="widget mobile-menu-widget %2$s">',
//			'after_widget'  => '</div>',
//			'before_title'  => '<h3 class="title">',
//			'after_title'   => '</h3>',
//		) );
	}

	add_action( 'widgets_init', 'hm_master_widgets_init' );
