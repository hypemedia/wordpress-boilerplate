<?php
	/**
	 * Additional functions and definitions: Gallery
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @since 1.1.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	add_action( 'init', 'hm_master_override_gallery' );
	
	/**
	 * Remove default shortcode and
	 * add custom function
	 */
	function hm_master_override_gallery() {
		// Remove default shortcode
		remove_shortcode( 'gallery' );
		
		// Add custom gallery
		add_shortcode( 'gallery', 'hm_master_gallery_shortcode' );
	}
	
	/**
	 * The custom gallery shortcode (slider)
	 *
	 * @param $atts
	 * @param $content
	 *
	 * @return string
	 */
	function hm_master_gallery_shortcode( $atts, $content ) {
		$ids = '';
		
		$attrs = shortcode_atts( array(
			'ids' => ''
		), $atts );
		extract( $attrs );
		
		// Iterate over attribute array, cleanup and make the array elements JavaScript ready
		foreach ( $attrs as $key => $attr ) {
			// Remove unnecessary array elements
			if ( in_array( $key, array( 'ids' ) ) || strpos( $key, '_' ) !== false ) {
				unset( $attrs[ $key ] );
			}
		}
		
		// keep only the real numeric ids
		$mapped_ids = array_filter( array_map( function ( $id ) {
			return (int) $id;
		}, explode( ',', $ids ) ) );
		
		// Create an empty variable for return html content
		$html_output = '';
		
		// Build the html slider structure (open)
		$html_output .= '<div class="wp-gallery">';
		
		// Iterate over the comma separated list of image ids
		foreach ( $mapped_ids as $media_id ) {
			if ( $image_data = wp_get_attachment_image_src( $media_id, 'card_md' ) ) {
				$html_output .= '<figure class="image-item"><img src="' . esc_url( $image_data[0] ) . '" class="img-fluid" /></figure>';
				$html_output .= '</div>';
			}
		}
		
		// Close the html slider structure and return the html output
		return $html_output . '</div>';
	}
