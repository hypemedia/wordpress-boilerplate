<?php
	/**
	 * Additional functions and definitions: Theme
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @version 1.1.0
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */


	function hm_master_cookie_bar(){ ?>
        <script>
            var hmcConfig = {
                cookieExpiresAfterDays: 30,
                privacyPolicy: '<?php echo get_privacy_policy_url(); ?>',
                default: false,
                mustConsent: false,
                acceptAll: true,
                cookieName: 'hmconsent',
                hideDeclineAll: true,
                hideLearnMore: false,
                brandColor: "#e3000b",
                noticeLayout: "bottom",
                hidePoweredBy: false,
                modalLayoutAlt: true,
                showToggleAll: true,
                lang: 'de',
                translations: {
                    de: {
                        consentModal: {
                            title: 'Informationen, die wir speichern',
                            description: 'Unsere Website nutzt Cookies, Analyse-Tools und externe Medien. Hier können Sie Ihre Einstellungen dazu treffen. Die Auswahl der Optionen stellt eine Einwilligung dar.',
                        },
                        shop: {
                            title: 'Informationen, die wir speichern',
                            description: 'Wir verwenden Cookies um an Sie und Ihre Präferenzen zu "erinnern", entweder für einen einzigen Besuch durch ein "Sitzungs-Cookie" oder für mehrere wiederholte Besuche durch ein "dauerhaftes Cookie". Sie gewährleisten eine konsistente und effiziente Erfahrung für Besucher und erfüllen wesentliche Funktionen, wie z.B. die Registrierung und das Eingeloggtbleiben der Benutzer sowie Inhalte Ihres Warenkorbs.',
                        },
                        matomo: {
                            description: 'Wir verwenden Matomo, um Ihr Nutzungsverhalten zu analysieren und somit die Nutzung unserer Website fortlaufend verbessern zu können.',
                        },
                        site: {
                            description: 'Einige Cookies werden verwendet, wenn Sie sich registrieren oder anmelden, um Ihre Daten zu speichern.',
                        },
                        leadinsight: {
                            description: 'Mit leadinsight werten wir anonyme Besucherstatistiken aus, um unsere angebotenen Dienste zu verbessern.',
                        },
                        externalMedia: {
                            description: 'Wir haben den Dienst YouTube und Goolge Maps eingebunden, um Ihnen ein interaktiveres Erlebnis auf unserer Website zu bieten. Mit der Aktivierung von YouTube und Google Maps erklären Sie sich auch mit der Übermittlung und Verarbeitung Ihrer Daten in den USA einverstanden. Diese kann mit Risiken für die Sicherheit Ihrer Daten verbunden sein.',
                        },
                        purposes: {
                            analytics: {
                                title: 'Besucher-Statistiken'
                            },
                            security: {
                                title: 'Sicherheit'
                            },
                            livechat: {
                                title: 'Livechat'
                            },
                            advertising: {
                                title: 'Werbung'
                            },
                            styling: {
                                title: 'Seitengestaltung'
                            },
                            payment: {
                                title: 'Bezahlung'
                            },
                            system: {
                                title: 'System',
                            },
                            externalMedia: {
                                title: 'Eingebettete Inhalte',
                            }
                        },
                    },
                    en: {
                        consentModal: {
                            description: 'Our website uses Cookies, analysis tools and external media. You can find the settings in that respect here. Selecting the options constitutes the granting of consent.',
                        },
                        shop: {
                            description: 'We use cookies that are used to ‘remember’ you and your preferences, either for a single visit through a ’session cookie’ or for multiple repeat visits using a ‘persistent cookie’. They ensure a consistent and efficient experience for visitors, and perform essential functions such as allowing users to register and remain logged in.',
                        },
                        matomo: {
                            description: 'We use Matomo to analyse your use behaviour and therefore constantly improve the use of our website.',
                        },
                        site: {
                            description: 'Some cookies are used when you register or login to store your information.',
                        },
                        leadinsight: {
                            description: 'Service for evaluating visitor statistics.',
                        },
                        externalMedia: {
                            description: 'We have incorporated the YouTube and Google Maps service to offer you an interactive experience on our website. By activating YouTube and Google Maps, you state that you consent to the forwarding and processing of your data in the USA. This may be associated with risks for the security of your data.',
                        },
                        purposes: {
                            analytics: {
                                title: 'Analytics'
                            },
                            security: {
                                title: 'Security'
                            },
                            livechat: {
                                title: 'Livechat'
                            },
                            advertising: {
                                title: 'Advertising'
                            },
                            styling: {
                                title: 'Styling'
                            },
                            payment: {
                                title: 'Payment'
                            },
                            system: {
                                title: 'System',
                            },
                            externalMedia: {
                                title: 'Embedded Content',
                            }
                        },
                    },
                },
                apps: [
                    {
                        name: 'site',
                        default: false,
                        title: 'Notwendige Cookies',
                        purposes: ['system'],
                        required: true,
                        optOut: false,
                        onlyOnce: true,
                    },
                    {
                        name: 'matomo',
                        default: false,
                        title: 'Matomo/Piwik',
                        purposes: ['analytics'],
                        cookies: [
                            [/^_pk_.*$/, '/', 'analytics.hypmed.de'], //for the production version
                            [/^_pk_.*$/, '/', 'localhost'], //for the local version
                            'piwik_ignore',
                        ],
                        required: false,
                        optOut: false,
                        onlyOnce: true,
                    },
                    {
                        name: 'leadinsight',
                        title: 'leadinsight',
                        purposes: ['analytics'],
                        cookies: ['inline-tracker'],
                        optOut: false,
                    },
                    {
                        name: 'externalMedia',
                        title: 'YouTube/Google Maps',
                        purposes: ['externalMedia'],
                        optOut: false,
                        required: false,
                        default: false,
                        callback: function(consent, app) {var is_de = document.documentElement.lang.includes('de'); var str = {de: { heading: "Eingebettete Inhalte deaktivert", pref: "Anpassen", link: "Externe Seite öffenen" }, en: { heading: "External media blocked", pref: "Change preferences", link: "Open on external website" }}; var consentcheck = app.name + consent;if (consentcheck == "externalMediafalse") {var frames = document.querySelectorAll(".iframe-wrapper iframe");frames.forEach(frame => {var extlink = frame.getAttribute("data-src");var parent = frame.parentNode;parent.classList.add("blocked");parent.insertAdjacentHTML('afterbegin', "<div class='hmc-message-wrap'><p class='hmc-message'><strong>" + str[(is_de ? 'de' : 'en')]['heading'] +"</strong><a class='hmc-pref cm-btn cm-btn-success cm-btn-info cm-btn-accept' onclick='return hmc.show();'>" + str[(is_de ? 'de' : 'en')]['pref'] +"</a><a class='hmc-extlink cm-btn cm-btn-success cm-btn-info cm-btn-accept' target='_blank' href='" + extlink + "'>" + str[(is_de ? 'de' : 'en')]['link'] +"</a></p></div>")});} else {var frames = document.querySelectorAll(".iframe-wrapper");frames.forEach(frame => {frame.classList.remove("blocked");frame.removeChild(frame.childNodes[0]);});}},
                    },
                ],
            };
        </script>

        <script type="opt-in" data-type="text/javascript" data-name="matomo">
            var _paq = _paq || [];
            _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
            _paq.push(["setCookieDomain", "*.hm_master-profilblech.de"]);
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u="//analytics.hypmed.de/";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', 'ID_HERE']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <noscript><p class="mb-0"><img src="//analytics.hypmed.de/piwik.php?idsite=ID_HERE&rec=1" style="border:0;" alt=""/></p></noscript>
        <script type="opt-in" data-type="text/javascript" data-name="leadinsight">
            var _li_ = window._li_ || [];
            (function () {
                var u = "//leadinsight.de/";
                var d = "ID_HERE";
                _li_.push(d); var a = document, c = a.createElement("script"), b = a.getElementsByTagName("script")[0]; c.type = "text/javascript"; c.async = true; nc.defer = true; c.src = u + "hello.js?cid=" + d; b.parentNode.insertBefore(c, b);
            })();
        </script>
	<?php }

	add_action('wp_footer', 'hm_master_cookie_bar');


	// Adds the Cookie-Bar Script and Stylesheet to the Footer
	function hm_master_excerpt($words = 20, $append = '...') {
		return wp_trim_words(get_the_excerpt(), $words, $append);
	}

	// Responsive Embeds
	function hm_master_wrap_oembed($html){
		$html = preg_replace('/(width|height)="\d*"\s/', "", $html);
		return'<div class="embed-responsive">' . $html . '</div>';
	}
	add_filter( 'embed_oembed_html', 'hm_master_wrap_oembed', 10, 1);

	// Checks if Array Key isset and has value
	function hm_master_check_item($thing) {
		if(isset($thing) && !empty($thing)) {
			return true;
		}

		return false;
	}

	// Beautified debug dumps
	function hm_master_pretty_dump($dump, $heading = ''){
		echo '<pre class="dev">';
		if($heading !== '') {
			echo '<h3>' . $heading . '</h3>';
		}
		print_r($dump);
		echo '</pre>';
	}

	function hm_master_render_page_logo($img_id = '', $img_class = '') {
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

		if ( has_custom_logo() ) {
			echo '<img height="80" id="' . $img_id . '" class="' . $img_class . '" alt="' . get_bloginfo( 'name' ) . '" src="'. esc_url( $logo[0] ) .'">';
		} else {
			echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
		}
	}

	//Disable gutenberg style in Front
	function hm_master_deregister_styles() {
		wp_dequeue_style( 'wp-block-library' );
	}
	add_action( 'wp_print_styles', 'hm_master_deregister_styles', 100 );

	/**
	 * Echos the link target
	 *
	 * @param string $target
	 */
	function hm_master_link_target( $target = '_self' ) {
		echo hm_master_get_link_target( $target );
	}

	/**
	 * Generates target and rel attrs
	 * for links
	 *
	 * @param string $target
	 *
	 * @return string
	 */
	function hm_master_get_link_target( $target = '_self' ) {
		if ( $target === 'blank' || $target === '_blank' || $target === '__blank' ) {
			return 'target="' . $target . '" rel="noopener"';
		}

		return '';
	}


	/**
	 *
	 * @param $settings
	 *
	 * @return string
	 */
	function hm_master_row_paddings( $settings ) {
		$top_str    = false;
		$bottom_str = false;

		if ( gettype( $settings['top'] ) == 'string' ) {
			$top_str    = $settings['top'];
			$bottom_str = $settings['bottom'];
		} else if ( gettype( $settings['top'] ) == 'array' ) {
			$top_str    = $settings['top'][0];
			$bottom_str = $settings['bottom'][0];
		}

		$t_sub = substr( $top_str, - 1 );
		$b_sub = substr( $bottom_str, - 1 );

		if ( intval( $t_sub ) == intval( $b_sub ) ) {
			return 'py-' . substr( $top_str, - 1 );
		}

		return 'pt-' . $t_sub . ' ' . 'pb-' . $b_sub;
	}

	/**
	 * Generates attributes for the <section>-Tag
	 * on the widget element.
	 *
	 * @param array $background
	 * @param array $otherClasses
	 * @param bool $dumpAttrs
	 * @param array $moreAttrs
	 * @param bool $force_overlay
	 *
	 * @return string
	 */
	function hm_master_section_attrs( $background = array(), $otherClasses = array(), $dumpAttrs = false, $moreAttrs = array(), $force_overlay = false ) {
		$attrs       = array();
		$attr_string = '';

		// Check $otherClasses
		if ( hm_master_check_item( $otherClasses ) ) {
			$attrs['class'] = '';

			foreach ( $otherClasses as $class ) {
				if ( $class ) {
					$attrs['class'] = $attrs['class'] . ' ' . $class;
				}
			}

			// Trim fist white space from string
			$attrs['class'] = ltrim( $attrs['class'] );
		}

		// Check $background
		if ( hm_master_check_item( $background ) ) {
			$textcolor = '';
			if ( array_key_exists( 'bg_overlay', $background ) ) {
				$backgroundoverlay = $background['bg_overlay'];
			} else {
				$backgroundoverlay = '';
			}


			if ( $background['type'] === 'color' ) {
				$backgroundcolor = $background['bg_color'];
				if ( array_key_exists( 'textcolor', $background ) && hm_master_check_item( $background['textcolor'] ) ) {
					$textcolor .= ' ' . $background['textcolor'];
				}

				if ( $force_overlay && array_key_exists( 'bg_overlay', $background ) ) {
					if ( is_array( $background['bg_overlay'] ) ) {
						$background['bg_overlay'] = $background['bg_overlay'][0];
					}

					if ( strpos( $background['bg_overlay'], 'overlay-gradient' ) !== false ) {
						$backgroundoverlay .= " overlay-gradient";
						$attrs['class']    = $attrs['class'] . ' background-full ' . $backgroundoverlay;
					}
				}

				$attrs['class'] = $attrs['class'] . ' ' . $backgroundcolor . $textcolor;
			} else if ( $background['type'] === 'image' ) {
				$backgroundimage = $background['bg_image'];

				if ( is_array( $background['bg_overlay'] ) ) {
					$background['bg_overlay'] = $background['bg_overlay'][0];
				}

				if ( strpos( $background['bg_overlay'], 'overlay-gradient' ) !== false ) {
					$backgroundoverlay .= " overlay-gradient";
				}
				
				$attrs['class'] = $attrs['class'] . ' background-full ' . $backgroundoverlay;
				
				$attrs['style'] = 'background-image:url(' . $backgroundimage['url'] . ');';
			}
		}

		// Check $moreAttrs
		if ( hm_master_check_item( $moreAttrs ) ) {
			foreach ( $moreAttrs as $key => $value ) {
				if ( gettype( $value ) === 'string' ) {
					if ( array_key_exists( $key, $attrs ) ) {
						$attrs[ $key ] = $attrs[ $key ] . ' ' . $value;
					} else {
						$attrs[ $key ] = $value;
					}
				}
			}
		}

		// Loop through attrs
		foreach ( $attrs as $key => $value ) {
			$attr_string .= ' ' . $key . '="' . $value . '"';
		}

		// DEV dump attrs
		if ( $dumpAttrs ) {
			hm_master_pretty_dump( $attrs );
		}

		return $attr_string;
	}

	/**
	 * Returns any string to id-attr format
	 *
	 * @param $str
	 *
	 * @return string|string[]|null
	 */
	function hm_master_str_to_id( $str ) {
		return preg_replace( '/\W+/', '', strtolower( strip_tags( $str ) ) );
	}

	/**
	 * @param $size
	 * @param string $colsize
	 * @param string $breakpoint
	 *
	 * @return string
	 */
	function hm_master_wysiwyg_column_switch( $size, $colsize = '8', $breakpoint = 'md' ) {
		if ( gettype( $breakpoint ) === 'array' ) {
			$breakpoint = $breakpoint[0];
		}

		switch ( $size ) {
			case 'left':
				$sizeclass = 'col-12 col-' . $breakpoint . '-' . $colsize;
				break;
			case 'center':
				$sizeclass = 'col-12 col-' . $breakpoint . '-' . $colsize . ' mx-auto';
				break;
			case 'right':
				$sizeclass = 'col-12 col-' . $breakpoint . '-' . $colsize . ' ml-auto';
				break;
			default:
				$sizeclass = 'col-12 col-' . $breakpoint . '-' . $colsize;
		}

		return $sizeclass;
	}

	/**
	 * Adds custom queryvars
	 *
	 * @param $vars
	 *
	 * @return array
	 */
	function hm_master_add_query_vars_filter( $vars ) {
		$vars[] = "return";

		return $vars;
	}

	add_filter( 'query_vars', 'hm_master_add_query_vars_filter' );

	/**
	 * Build the entire current page URL (incl query strings) and output it
	 * Useful for social media plugins and other times you need the full page URL
	 * Also can be used outside The Loop, unlike the_permalink
	 *
	 * @param bool $remove_qs
	 *
	 * @return string
	 */
	function hm_master_get_current_page_url( $remove_qs = false ) {
		global $wp;

		if ( $remove_qs ) {
			return home_url( $wp->request );
		} else {
			return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
		}
	}


	/**
	 * Shorthand for echo hm_master_get_current_page_url();
	 *
	 * @param bool $remove_qs
	 */
	function hm_master_the_current_page_url( $remove_qs = false ) {
		echo hm_master_get_current_page_url();
	}

	/**
	 * Get Weekday by input
	 *
	 * @param $input
	 * @param $char
	 */
	function hm_master_get_weekday( $input, $char ) {
		$date = date_create( $input );
		setlocale( LC_TIME, array( 'de_DE.UTF-8', 'de_DE', 'de-DE', 'de', 'german', 'German' ) );
		echo strftime( $char, $date->getTimestamp() );
	}

	/**
	 * Calculate the days until
	 * a given date
	 *
	 * @param $input
	 *
	 * @return false|float
	 */
	function hm_master_get_days_until( $input ) {
		$now = time();

		$your_date = strtotime( $input );
		$datediff  = $your_date - $now;

		return round( $datediff / ( 60 * 60 * 24 ) );
	}

	/**
	 * Echo days until
	 *
	 * @param $input
	 */
	function hm_master_days_until( $input ) {
		echo hm_master_get_days_until( $input );
	}

	/**
	 * Get remaining days
	 * as formatted string
	 *
	 * @param $start
	 *
	 * @return string
	 */
	function hm_master_get_remaining_days( $start ) {
		$remaining_days = hm_master_get_days_until( $start ) + 1;

		if ( $remaining_days >= 1 ) {
			if ( $remaining_days == 1 ) {
				return 'Findet Morgen statt';
			} else {
				return 'Findet in ' . $remaining_days . ' Tagen statt';
			}
		} else {
			return 'Event findet gerade statt';
		}
	}

	/**
	 * Check if date is past
	 *
	 * @param $input
	 *
	 * @return bool
	 */
	function hm_master_is_past( $input ) {
		$is_past = false;
		if ( hm_master_get_days_until( $input ) < 0 ) {
			$is_past = true;
		}

		return $is_past;
	}

	/**
	 * Get the formatted date
	 *
	 * @param $in
	 * @param $field
	 * @param $out
	 *
	 * @return string
	 */
	function hm_master_get_formatted_date( $in, $field, $out ) {
		$date = DateTime::createFromFormat( $in, get_field( $field ) );

		return date_i18n( $out, $date->getTimestamp() );
	}

	/**
	 * Echo the formatted date
	 *
	 * @param $in
	 * @param $field
	 * @param $out
	 */
	function hm_master_the_formatted_date( $in, $field, $out ) {
		echo hm_master_get_formatted_date( $in, $field, $out );
	}

	/**
	 * @param $location
	 * @param array $args
	 *
	 * @return array|bool|false
	 */
	function hm_master_get_nav_menu_items_by_location( $location, $args = [] ) {

		// Get all locations
		$locations = get_nav_menu_locations();

		// Get object id by location
		$object = wp_get_nav_menu_object( $locations[ $location ] );

		// Get menu items by menu name
		$menu_items = wp_get_nav_menu_items( $object->name, $args );

		// Return menu post objects
		return $menu_items;
	}

	/**
	 * Add content after <body>
	 */
	function hm_master_add_custom_body_open_code() { ?>
		<!--[if IE lt 11]><?php echo sprintf(__( '<div class="alert">Sie benutzen einen <strong>veralteten</strong> Web-Browser. <a href="%s" target="_blank" rel="noopener">Bitte aktualisieren Sie Ihren Browser</a>, um die neusten Funktionen zu nutzen und sicher zu surfen.</div>', 'hm_master' ), esc_url( 'https://browsehappy.com/' )); ?><![endif]-->
        <noscript><div class="alert"><?php echo sprintf( __('Für einige Funktionen dieser Webseite sollten Sie <a href="%s" target="_blank" rel="noopener">JavaScript in Ihrem Browser aktivieren.</a>', 'hm_master'), esc_url('https://www.enable-javascript.com/de/'));?></div></noscript>

        <div class="loading"><div class="logo-icon animate-spin"></div></div>
	<?php }
	add_action( 'wp_body_open', 'hm_master_add_custom_body_open_code' );
