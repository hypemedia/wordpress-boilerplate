<?php
	/**
	 * Custom Boostrap Pagination
	 *
	 * USAGE:
	 *     <?php echo hm_master_bootstrap_pagination(); ?> //uses global $wp_query
	 *
	 * or with custom WP_Query():
	 *     <?php $query = new \WP_Query($args);
	 *       ... while(have_posts()), $query->posts stuff ...
	 *       echo hm_master_bootstrap_pagination($query);
	 *     ?>
	 *
	 */
	
	/**
	 * @param string $fragment
	 * @param WP_Query|null $wp_query
	 * @param bool $echo
	 *
	 * @return string|null
	 */
	function hm_master_bootstrap_pagination( $fragment = '', \WP_Query $wp_query = null, $echo = true ) {
		if ( null === $wp_query ) {
			global $wp_query;
		}
		$pages = paginate_links( [
				'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				'format'       => '?paged=%#%',
				'current'      => max( 1, get_query_var( 'paged' ) ),
				'total'        => $wp_query->max_num_pages,
				'type'         => 'array',
				'show_all'     => false,
				'end_size'     => 3,
				'mid_size'     => 1,
				'prev_next'    => true,
				'prev_text'    => '<i class="hm-icon-angle-left"></i>',
				'next_text'    => '<i class="hm-icon-angle-right"></i>',
				'add_args'     => false,
				'add_fragment' => $fragment
			]
		);
		
		if (is_array($pages)) {
			//$paged = ( get_query_var( 'paged' ) == 0 ) ? 1 : get_query_var( 'paged' );
			$pagination = '<nav aria-label="' . __('Aktuelles', 'hm_master')  .'" class="pagination-wrapper"><ul class="pagination justify-content-center mb-0">';
			foreach ( $pages as $page ) {
				$pagination .= '<li class="page-item"> ' . str_replace( 'page-numbers', 'page-link', $page ) . '</li>';
			}
			$pagination .= '</ul></nav>';
			if ( $echo ) {
				echo $pagination;
			} else {
				return $pagination;
			}
		}
		return null;
	}
	
	/**
	 * Prints the pagination HTML
	 *
	 * @param string $fragment
	 * @param $query
	 */
	function hm_master_print_pagination($fragment = '', $query = null) {
		echo hm_master_bootstrap_pagination($fragment, $query);
	}
	
	/**
	 * Return pagination only if there are more
	 * than 1 page-
	 * @return bool
	 */
	function hm_master_show_bootstrap_pagination() {
		global $wp_query;
		return ( $wp_query->max_num_pages > 1 );
	}
