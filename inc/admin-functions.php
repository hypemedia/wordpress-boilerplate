<?php
	/**
	 * Additional functions and definitions: Admin
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */

	// Add Editor-Styles
	add_editor_style( get_stylesheet_directory_uri() . '/assets/dist/css/editor.css' );

	// Add Admin-Stylesheet
	function hm_master_admin_stylesheet() {
		wp_enqueue_style( 'admin-styles', get_stylesheet_directory_uri() . '/assets/dist/css/admin.css' );
	}

	add_action( 'admin_enqueue_scripts', 'hm_master_admin_stylesheet' );

	// Add ACF-Options Page
	//	if( function_exists('acf_add_options_page') ) {
	//		acf_add_options_page();
	//	}

	// Image-Meta on upload
	function hm_master_set_image_meta( $post_ID ) {
		if ( wp_attachment_is_image( $post_ID ) ) {
			$my_image_title = get_post( $post_ID )->post_title;
			$my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );
			$my_image_title = ucwords( strtolower( $my_image_title ) );
			$my_image_meta = array(
				'ID'		=> $post_ID,
				'post_title'	=> $my_image_title . ' - ' . get_bloginfo('name'),
				'post_excerpt'	=> $my_image_title . ' - ' . get_bloginfo('name'),
				'post_content'	=> $my_image_title . ' - ' . get_bloginfo('name'),
			);

			update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );
			wp_update_post( $my_image_meta );
		}
	}
	add_action( 'add_attachment', 'hm_master_set_image_meta' );

	// Allows upload of SVG files
	function hm_master_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
	add_filter('upload_mimes', 'hm_master_mime_types');

	// Update Column Width Admin Category
	add_action('admin_head', 'hm_master_admin_category_column_width');
	function hm_master_admin_category_column_width() {
		echo '<style type="text/css">';
		echo 'form#edittag { max-width: 1200px; }';
		echo '</style>';
	}

	/**
	 * TinyMCE-Editor Custom Styles
	 *
	 * @param $buttons
	 *
	 * @return mixed
	 */
	function hm_master_tinymce_editor_button( $buttons ) {
		array_unshift( $buttons, 'styleselect' );

		return $buttons;
	}

	add_filter( 'mce_buttons_2', 'hm_master_tinymce_editor_button' );

	/**
	 * Add TinyMCE-Editor Buttons
	 *
	 * @param $settings
	 *
	 * @return mixed
	 */
	function hm_master_tinymce_before_init( $settings ) {
		$style_formats = array(
			// Text
			array(
				'title' => 'Fließtext',
				'items' => array(
					array(
						'title'    => 'Lead-Paragraph',
						'selector' => 'p',
						'classes'  => 'lead'
					),
					array(
						'title'   => 'Kleiner Text',
						'inline'  => 'span',
						'classes' => 'small'
					)
				)
			),
			// Headings
			array(
				'title' => 'Überschriften',
				'items' => array(
					array(
						'title'    => 'Überschrift-Größe 1',
						'selector' => 'h1,h2,h3,h4,h5,h6',
						'classes'  => 'h1'
					),
					array(
						'title'    => 'Überschrift-Größe 2',
						'selector' => 'h1,h2,h3,h4,h5,h6',
						'classes'  => 'h2'
					),
					array(
						'title'    => 'Überschrift-Größe 3',
						'selector' => 'h1,h2,h3,h4,h5,h6',
						'classes'  => 'h3'
					),
					array(
						'title'    => 'Überschrift-Größe 4',
						'selector' => 'h1,h2,h3,h4,h5,h6',
						'classes'  => 'h4'
					),
					array(
						'title'    => 'Überschrift-Größe 5',
						'selector' => 'h1,h2,h3,h4,h5,h6',
						'classes'  => 'h5'
					),
					array(
						'title'    => 'Überschrift-Größe 6',
						'selector' => 'h1,h2,h3,h4,h5,h6',
						'classes'  => 'h6'
					),
				)
			),
			array(
				'title' => 'Wrapper',
				'items' => array(
					array(
						'title' => 'Block-Element',
						'block' => 'div',
						'classes' => 'd-block',
						'wrapper' => true
					),
					array(
						'title'    => 'Auto-Abstand Oben',
						'block' => 'div',
						'classes'  => 'mt-auto',
						'wrapper' => true
					),
					array(
						'title'    => 'Auto-Abstand Unten',
						'block' => 'div',
						'classes'  => 'mb-auto',
						'wrapper' => true
					),
					array(
						'title'    => 'Auto-Abstand Oben + Unten',
						'block' => 'div',
						'classes'  => 'mt-auto mb-auto',
						'wrapper' => true
					),
					array(
						'title'    => 'Auto-Abstand Links + Rechts',
						'block' => 'div',
						'classes'  => 'mx-auto',
						'wrapper' => true
					),
				)
			),
			// Buttons
			array(
				'title' => 'Buttons & Links',
				'items' => array(
					array(
						'title'    => 'Button Primär',
						'selector' => 'a',
						'classes'  => 'btn btn-primary'
					),
					array(
						'title'    => 'Button Sekundär',
						'selector' => 'a',
						'classes'  => 'btn btn-secondary'
					),
					array(
						'title'    => 'Button Hellgrau',
						'selector' => 'a',
						'classes'  => 'btn btn-light'
					),
					array(
						'title'    => 'Button Dunkelgrau',
						'selector' => 'a',
						'classes'  => 'btn btn-dark'
					),
					array(
						'title'    => 'Button Klein',
						'selector' => 'a',
						'classes'  => 'btn-sm'
					),
					array(
						'title'    => 'Button Groß',
						'selector' => 'a',
						'classes'  => 'btn-lg'
					),
					array(
						'title'    => 'Link Styled (Light)',
						'selector' => 'a',
						'classes'  => 'link-styled'
					),
					array(
						'title'    => 'Link Styled (Dark)',
						'selector' => 'a',
						'classes'  => 'link-styled link-styled-dark'
					),
				)
			),
			// Text color
			array(
				'title' => 'Textfarben',
				'items' => array(
					array(
						'title'   => 'Textfarbe Muted',
						'inline'  => 'span',
						'classes' => 'text-muted'
					),
					array(
						'title'   => 'Textfarbe Primär',
						'inline'  => 'span',
						'classes' => 'text-primary'
					),
					array(
						'title'   => 'Textfarbe Sekundär',
						'inline'  => 'span',
						'classes' => 'text-secondary'
					),
				)
			),
			// Helpers
			array(
				'title' => 'Hilfsklassen',
				'items' => array(
					array(
						'title'    => 'Block-Element',
						'selector' => '*',
						'classes'  => 'd-block'
					),
					array(
						'title'    => 'Auto-Abstand Oben',
						'selector' => '*',
						'classes'  => 'mt-auto'
					),
					array(
						'title'    => 'Auto-Abstand Unten',
						'selector' => '*',
						'classes'  => 'mb-auto'
					),
					array(
						'title'    => 'Auto-Abstand Oben + Unten',
						'selector' => '*',
						'classes'  => 'mt-auto mb-auto'
					),
					array(
						'title'    => 'Auto-Abstand Links + Rechts',
						'selector' => '*',
						'classes'  => 'mx-auto'
					),
					array(
						'title'    => 'Wortumbruch mit Bindestrich',
						'selector' => '*',
						'classes'  => 'hyphenate'
					),
				)
			),
			// Margins
			array(
				'title' => 'Abstände',
				'items' => array(
					array(
						'title'    => 'Abstand Oben-1',
						'selector' => '*',
						'classes'  => 'mt-1'
					),
					array(
						'title'    => 'Abstand Oben-2',
						'selector' => '*',
						'classes'  => 'mt-2'
					),
					array(
						'title'    => 'Abstand Oben-3',
						'selector' => '*',
						'classes'  => 'mt-3'
					),
					array(
						'title'    => 'Abstand Oben-4',
						'selector' => '*',
						'classes'  => 'mt-4'
					),
					array(
						'title'    => 'Abstand Oben-5',
						'selector' => '*',
						'classes'  => 'mt-5'
					),
					array(
						'title'    => 'Abstand Unten-1',
						'selector' => '*',
						'classes'  => 'mb-1'
					),
					array(
						'title'    => 'Abstand Unten-2',
						'selector' => '*',
						'classes'  => 'mb-2'
					),
					array(
						'title'    => 'Abstand Unten-3',
						'selector' => '*',
						'classes'  => 'mb-3'
					),
					array(
						'title'    => 'Abstand Unten-4',
						'selector' => '*',
						'classes'  => 'mb-4'
					),
					array(
						'title'    => 'Abstand Unten-5',
						'selector' => '*',
						'classes'  => 'mb-5'
					),
				)
			),
			// Developer
			array(
				'title' => 'Medien',
				'items' => array(
					array(
						'title'    => 'Schatten',
						'selector' => '*',
						'classes'  => 'shadow'
					)
				)
			),
			// Developer
			array(
				'title' => 'Entwickler',
				'items' => array(
					array(
						'title'    => 'align-self-start',
						'selector' => '*',
						'classes'  => 'align-self-start'
					),
					array(
						'title'    => 'align-self-end',
						'selector' => '*',
						'classes'  => 'align-self-end'
					),
				)
			),
		);

		$settings['style_formats'] = json_encode( $style_formats );

		return $settings;
	}

	add_filter( 'tiny_mce_before_init', 'hm_master_tinymce_before_init' );


	/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
	function hm_master_duplicate_post_as_draft() {
		global $wpdb;
		if ( ! ( isset( $_GET['post'] ) || isset( $_POST['post'] ) || ( isset( $_REQUEST['action'] ) && 'hm_master_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
			wp_die( 'No post to duplicate has been supplied!' );
		}

		/*
		 * Nonce verification
		 */
		if ( ! isset( $_GET['duplicate_nonce'] ) || ! wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) ) {
			return;
		}

		/*
		 * get the original post id
		 */
		$post_id = ( isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
		/*
		 * and all the original post data then
		 */
		$post = get_post( $post_id );

		/*
		 * if you don't want current user to be the new post author,
		 * then change next couple of lines to this: $new_post_author = $post->post_author;
		 */
		$current_user    = wp_get_current_user();
		$new_post_author = $current_user->ID;

		/*
		 * if post data exists, create the post duplicate
		 */
		if ( isset( $post ) && $post != null ) {

			/*
			 * new post data array
			 */
			$args = array(
				'comment_status' => $post->comment_status,
				'ping_status'    => $post->ping_status,
				'post_author'    => $new_post_author,
				'post_content'   => $post->post_content,
				'post_excerpt'   => $post->post_excerpt,
				'post_name'      => $post->post_name,
				'post_parent'    => $post->post_parent,
				'post_password'  => $post->post_password,
				'post_status'    => 'draft',
				'post_title'     => $post->post_title,
				'post_type'      => $post->post_type,
				'to_ping'        => $post->to_ping,
				'menu_order'     => $post->menu_order
			);

			/*
			 * insert the post by wp_insert_post() function
			 */
			$new_post_id = wp_insert_post( $args );

			/*
			 * get all current post terms ad set them to the new post draft
			 */
			$taxonomies = get_object_taxonomies( $post->post_type ); // returns array of taxonomy names for post type, ex array("category", "post_tag");
			foreach ( $taxonomies as $taxonomy ) {
				$post_terms = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'slugs' ) );
				wp_set_object_terms( $new_post_id, $post_terms, $taxonomy, false );
			}

			/*
			 * duplicate all post meta just in two SQL queries
			 */
			$post_meta_infos = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id" );
			if ( count( $post_meta_infos ) != 0 ) {
				$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
				foreach ( $post_meta_infos as $meta_info ) {
					$meta_key = $meta_info->meta_key;
					if ( $meta_key == '_wp_old_slug' ) {
						continue;
					}
					$meta_value      = addslashes( $meta_info->meta_value );
					$sql_query_sel[] = "SELECT $new_post_id, '$meta_key', '$meta_value'";
				}
				$sql_query .= implode( " UNION ALL ", $sql_query_sel );
				$wpdb->query( $sql_query );
			}


			/*
			 * finally, redirect to the edit post screen for the new draft
			 */
			wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
			exit;
		} else {
			wp_die( 'Post creation failed, could not find original post: ' . $post_id );
		}
	}

	add_action( 'admin_action_hm_master_duplicate_post_as_draft', 'hm_master_duplicate_post_as_draft' );

	/*
	 * Add the duplicate link to action list for post_row_actions
	 */
	function hm_master_duplicate_post_link( $actions, $post ) {
		if ( current_user_can( 'edit_posts' ) ) {
			$actions['duplicate'] = '<a href="' . wp_nonce_url( 'admin.php?action=hm_master_duplicate_post_as_draft&post=' . $post->ID, basename( __FILE__ ), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Duplizieren</a>';
		}

		return $actions;
	}

	add_filter( 'post_row_actions', 'hm_master_duplicate_post_link', 10, 2 );
	add_filter( 'page_row_actions', 'hm_master_duplicate_post_link', 10, 2 );


	/**
	 * Excludes posts with hide_post_from_loop meta key from
	 * main loop
	 *
	 * @param $query
	 */
	function hm_master_exclude_featured_post( $query ) {
		if ( $query->is_home() && $query->is_main_query()) {
			// in case for some reason there's already a meta query set from other plugin
			$meta_query = $query->get('meta_query')? : [];

			// append yours
			$meta_query[] =
				[
					'relation' => 'OR',
					[
						'key' => 'hide_post_from_loop',
						'compare' => 'NOT EXISTS'
					],
					[
						'key' => 'hide_post_from_loop',
						'value' => '1',
						'compare' => '!='
					]
				];

			$query->set('meta_query', $meta_query);
		}
	}
	add_action( 'pre_get_posts', 'hm_master_exclude_featured_post' );

