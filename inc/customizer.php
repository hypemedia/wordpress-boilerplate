<?php
	/**
	 * Customizer Settings
	 *
	 * @package hm_master
	 * @since 1.3.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	/**
	 * Sanitize return value from
	 * dropdown pages
	 *
	 * @param $page_id
	 * @param $setting
	 *
	 * @return int|mixed
	 */
	function hm_master_sanitize_dropdown_pages( $page_id, $setting ) {
		// Ensure $input is an absolute integer.
		$page_id = absint( $page_id );
		
		// If $page_id is an ID of a published page, return it; otherwise, return the default.
		return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
	}
	
	/**
	 * Register customizer settings
	 * @param $wp_customize
	 */
	function hm_master_customize_register( $wp_customize ) {
		// Create our sections
		$wp_customize->add_section( 'hm_master_theme_options' , array(
			'title'             => __( 'Theme-Options', 'hm_master' ),
			'description'       => __( 'Impressum und Dataneschutz kann unter "Design>Menüs" bearbeitet werden', 'hm_master' )
		) );
		
		// Create our settings
		$wp_customize->add_setting( 'hm_master_theme_options_footer_column_1' , array(
			'type'          => 'theme_mod',
			'transport'     => 'refresh',
		) );
		$wp_customize->add_control( 'hm_master_theme_options_footer_column_1_control', array(
			'label'      => __( 'Spalte 1', 'hm_master' ),
			'section'    => 'hm_master_theme_options',
			'settings'   => 'hm_master_theme_options_footer_column_1',
			'type'       => 'textarea',
		) );
		
		$wp_customize->add_setting( 'hm_master_theme_options_footer_column_2' , array(
			'type'          => 'theme_mod',
			'transport'     => 'refresh',
		) );
		$wp_customize->add_control( 'hm_master_theme_options_footer_column_2_control', array(
			'label'      => __( 'Spalte 2', 'hm_master' ),
			'section'    => 'hm_master_theme_options',
			'settings'   => 'hm_master_theme_options_footer_column_2',
			'type'       => 'textarea',
		) );
		
		$wp_customize->add_setting( 'hm_master_theme_options_blog_fallback', array(
			'type'              => 'theme_mod',
			'default'           => '',
		) );
		
		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'hm_master_theme_options_blog_fallback', array(
			'label'   => __( 'Standard-Vorschaubild', 'hm_master' ),
			'section' => 'hm_master_theme_options',
			'settings'   => 'hm_master_theme_options_blog_fallback',
		) ) );
	}
	add_action( 'customize_register', 'hm_master_customize_register' );
