<?php
	/**
	 * The static header for our theme
	 *
	 * @package hm_master
	 * @version 1.0.1
	 * @author hype.media <web@hype-media.de>
	 */
	
	if ( function_exists( 'icl_get_languages' ) ) {
		$langs       = apply_filters( 'wpml_active_languages', null, 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
		$langs_count = count( $langs );
	} else {
		$langs          = array();
		$langs_count    = 0;
	}
	
	$main_menu_args = array(
		'theme_location'    => 'main',
		'depth'             => 2,
		'container'         => 'div',
		'container_class'   => 'collapse navbar-collapse',
		'container_id'      => 'header-main-menu',
		'menu_class'        => 'nav navbar-nav ml-auto',
		'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		'walker'            => new WP_Bootstrap_Navwalker(),
	);
	
	$image_area = $GLOBALS['HM_MASTER']['IMAGE_AREA'];
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="wrapper" class="image-area-<?php echo $image_area; ?>">

    <header id="masthead" class="header-top">
        <div class="header-inner">
            <nav class="header-main-menu navbar navbar-expand-lg navbar-light bg-white" role="navigation">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
		                <?php hm_master_render_page_logo( 'page-logo' ); ?>
                    </a>
                    
                    <!--button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#header-main-menu" aria-controls="header-main-menu" aria-expanded="false" aria-label="Menü öfhm_masteren"-->
                    <button class="navbar-toggler collapsed ml-auto" id="toggle-mobile-menu">
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
	
	                <?php if ( function_exists( 'icl_get_languages' ) ): ?>
		                <?php foreach ( $langs as $lang ) {
			                if ( ! $lang['active'] ) {
				                echo '<a class="lang-item btn btn-outline-light btn-sm lang-link text-body" href="' . $lang['url'] . '">' . $lang['native_name'] . '</a>';
			                }
		                } ?>
	                <?php endif; ?>
                 
				    <?php wp_nav_menu( $main_menu_args ); ?>
                </div>
            </nav>
        </div>
        <aside class="collapse bg-bc" id="search-bar">
            <div class="aside-inner container py-2">
			    <div class="row">
                    <div class="col-8 col-md-4 ml-auto">
	                    <?php get_template_part('/template-parts/forms/' . 'header-search-form'); ?>
                    </div>
                </div>
            </div>
        </aside>
    </header>
