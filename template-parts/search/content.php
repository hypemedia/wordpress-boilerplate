<?php
	/**
	 * The search content file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>
<div class="col-12">
    <article id="post-<?php the_ID(); ?>" class="article-item post-item archive card flex-column reveal shadow-sm mb-2">
        <div class="row no-gutters">
            <div class="col-md-9">
                <div class="card-body d-flex flex-column h-100">
                    <h2 class="card-title mb-0">
                        <a href="<?php the_permalink(); ?>" class="no-style" title="<?php the_title_attribute( array(
							'before' => __( 'Zum Artikel: ', 'hm_master' ),
							'after'  => ''
						) ); ?>"><?php the_title( '' ); ?></a></h2>
                    <div class="meta-info">
                        <time class="article-meta date small" datetime="<?php echo get_the_date( 'c' ); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
                    </div>
                    <div class="content mt-auto">
						<?php echo hm_master_excerpt( 25, 'mb-1 text-muted' ); ?>
                        <a href="<?php the_permalink(); ?>" class="read-more d-block"><?php _e( 'Weiterlesen »', 'hm_master' ); ?></a>
                    </div>
                </div>
            </div>
	        <?php if ( has_post_thumbnail() ): ?>
                <div class="col-md-3">
                    <a class="hover-link" href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url( $post, 'card_md' ); ?>" class="card-img-top" alt="<?php the_title_attribute( array(
					        'before' => __( 'Vorschaubild: ', 'hm_master' ),
					        'after'  => ''
				        ) ); ?>"> </a>
                </div>
	        <?php endif; ?>
        </div>
    </article>
</div>
