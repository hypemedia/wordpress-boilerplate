<?php
	/**
	 * The blog content file
	 *
	 * @package hm_master
	 * @version 1.3.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	global $post;
	
	$term = get_the_category( $post->ID );
?>


<section class="article-item single py-4">
    <div class="section-inner container">
        <div class="row">
            <div class="col-12">
                <article id="post-" class="article-item single">
                    <div class="content-wrap" itemprop="articleBody">
                        <div class="row">
                            <div class="col-12 col-md-7">
	                            <?php the_title('<h1 class="text-center text-md-left" itemprop="name">', '</h1>'); ?>
	
	                            <?php the_content(); ?>
                            </div>
                            <div class="col-12 col-md-5">
		                        <?php
			                        if ( has_post_thumbnail()) {
				                        echo "<figure class=\"card post-thumbnail\">";
				                        echo wp_get_attachment_image(get_post_thumbnail_id(), 'card_md', '', ['class' => 'img-fluid card-img',  'itemprop' => "image" ]);
				                        echo "<figcaption class=\"card-body text-center text-md-left\">" . get_the_post_thumbnail_caption() . "</figcaption>";
				                        echo "</figure>";
			                        };
		                        ?>
                            </div>
                            <div class="col-12">
                                <div class="text-center text-md-left">
                                    <a href="<?php echo get_category_link($term[0]->term_id); ?>" class="category-display badge badge-pill badge-primary" itemprop="genre"><?php echo $term[0]->name;  ?></a>
                                    <time class="date-display badge badge-pill badge-primary" itemprop="datePublished" datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('d.m.Y'); ?></time>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
