<?php
	/**
	 * The blog content file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	global $post;
	
	$term = get_the_category( $post->ID );
?>


    <article id="post-" class="article-item archive card border-0 shadow-sm mb-3 h-100">
        <?php
            if ( has_post_thumbnail() ):
                $thumbnail = get_post_thumbnail_id();
            else:
                $thumbnail = get_theme_mod( 'hm_master_theme_options_blog_fallback' );
            endif;
            
            if($thumbnail) echo '<a class="post-link" href="'. get_the_permalink() . '">' . wp_get_attachment_image($thumbnail, 'card_md', '', ['class' => 'img-full my-auto card-img-top']) . '</a>';
        ?>
    
        <div class="card-body mt-1">
            <a class="post-link" itemprop="url" href="<?php echo get_the_permalink(); ?>"><?php the_title('<h2 class="h3" itemprop="name">', '</h2>'); ?></a>
            <?php echo hm_master_excerpt( 25 ); ?>
        </div>
        <div class="card-footer">
            <a href="<?php echo get_category_link($term[0]->term_id); ?>" class="category-display badge badge-pill badge-primary" itemprop="genre"><?php echo $term[0]->name;  ?></a>
            <time class="date-display badge badge-pill badge-primary" itemprop="datePublished" datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('d.m.Y'); ?></time>
        </div>
    </article>
