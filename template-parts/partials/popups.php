<?php
	/**
	 * Template for Popups created with ACF
	 *
	 * @package hm_master
	 * @version 1.0.1
	 * @author hype.media <web@hype-media.de>
	 */
    
    if( function_exists( 'get_field' ) ) {
	    if ( have_rows( 'popups' ) ):
		    echo "<!-- MODALS -->";
		    while ( have_rows( 'popups' ) ): the_row();
			    $content = get_sub_field( 'content' );
			    ?>
                <aside class="modal fade" id="<?php echo $content['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $content['id']; ?>Label" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                        <div class="modal-content">
						    <?php if ( hm_master_check_item( $content['title'] ) ): ?>
                                <div class="modal-header pb-0">
                                    <h5 class="modal-title" id="<?php echo $content['id']; ?>Label"><?php echo $content['title']; ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
						    <?php endif; ?>
						    <?php if ( hm_master_check_item( $content['content'] ) ): ?>
                                <div class="modal-body">
								    <?php if ( !hm_master_check_item( $content['title'] ) ): ?>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
								    <?php endif; ?>
								    <?php echo $content['content']; ?>
                                </div>
						    <?php endif; ?>
                        </div>
                    </div>
                </aside>
		    <?php
		    endwhile;
		    echo "<!-- /MODALS -->";
	    endif;
    }
?>


