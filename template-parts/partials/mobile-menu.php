<?php
    /**
     * The mobile menu template
     *
     * @package hm_master
     * @version 1.0.0
     * @author hype.media <web@hype-media.de>
     */

    $mobile_menu_args = array(
        'theme_location'    => 'mobile',
        'depth'             => 2,
        'container'         => false,
        'container_class'   => '',
    );


    if( function_exists( 'get_field' ) ) {
        $show_search =  get_field('show_search', 'options');
    } else {
        $show_search = false;
    }
?>


<aside class="modal fade show" id="mobile-nav" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-nav-body">
            <div class="modal-nav-content">
                <div class="navbar-collapse">
                    <div class="wrap mb-2">
                        <div class="inner menu container">
                            <?php
                                // Display mobile nav
                                if(has_nav_menu( 'mobile' )) {
                                    wp_nav_menu( $mobile_menu_args );
                                }

                                // Display search form
                                if( $show_search ) {
                                    echo "<hr>";
                                    echo get_template_part('template-parts/forms/search-form');
                                }

                                // Display widgets
                                if( is_active_sidebar('mobile-menu-1') ) {
                                    echo "<hr>";
                                    dynamic_sidebar('mobile-menu-1');
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>
