<?php
	/**
	 * Template static top-image-area
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	$background = array(
		'type'          => 'color',
		'bg_color'      => 'bg-light'
	);
	
	$is_home = get_field( 'is-home' ) ? : false; // Bool
	
	// hero-section classes
	$classes = array(
		'image-area-section',
		'image-area-slide',
		'intro',
		$is_home ? 'is-home' : 'is-page'
	);
	
	$height = "50";
	
	$section_attrs = hm_master_section_attrs( $background, $classes, false);
?>
<!-- IMAGE-AREA-SLIDE -->
<section <?php echo $section_attrs; ?>>
    <div class="section-inner container-full position-relative">
        <div class="image-track hero-slide">
			<?php
				while ( have_rows( 'slides' ) ):
					the_row();
					$content = get_sub_field( 'slide' ); // Array
                    
                    $background_color   = $content["backgroundcolor"];
                    $text_color         = $content["textcolor"];
					
                    if(hm_master_check_item($content['image'])) {
	                    $slide_background = array(
		                    'type'       => 'image',
		                    'bg_image'   => $content['image'],
		                    'bg_overlay' => $content['hide_overlay'] ? '' : 'overlay overlay-dark text-white'
	                    );
	
	                    // hero-section classes
	                    $slide_classes = array(
		                    'image-item',
		                    'd-flex',
		                    'flex-column',
		                    'justify-content-end',
		                    'position-relative',
	                    );
                    } else {
	                    $slide_background = array(
		                    'type'          => 'color',
		                    'bg_color'      => $background_color,
		                    'textcolor'     => $text_color,
		                    'bg_overlay'    => $content['hide_overlay'] ? '' : 'overlay overlay-dark text-body'
	                    );
	
	                    // hero-section classes
	                    $slide_classes = array(
		                    'slide-item',
		                    'd-flex',
		                    'flex-column',
		                    'justify-content-end',
		                    'position-relative',
	                    );
                    }
					
					$slide_attrs = hm_master_section_attrs($slide_background, $slide_classes, false);
					
					?>
                    <div <?php echo $slide_attrs; ?>>
                        <div class="image-item-inner mb-3">
                            <div class="container">
                                <div class="row no-gutters">
                                    <div class="col-12 col-md-6">
                                        <div class="content <?php echo hm_master_check_item($content['image']) ? 'text-white ' : '' ?>mb-2 mb-md-5">
		                                    <?php echo $content['text']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
        </div>
    </div>
</section>
<!-- /IMAGE-AREA-SLIDE -->

