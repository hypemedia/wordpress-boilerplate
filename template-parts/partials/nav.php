<?php
    /**
     * The header-inner/shared content (e.g. nav)
     *
     * @package hm_master
     * @version 1.0.0
     * @author hype.media <web@hype-media.de>
     */

    if ( function_exists( 'icl_get_languages' ) ) {
        $langs       = apply_filters( 'wpml_active_languages', null, 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
        $langs_count = count( $langs );
    } else {
        $langs          = array();
        $langs_count    = 0;
    }

    $main_menu_args = array(
        'theme_location'    => 'main',
        'depth'             => 2,
        'container'         => 'div',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'header-main-menu',
        'menu_class'        => 'nav navbar-nav ml-auto',
        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
        'walker'            => new WP_Bootstrap_Navwalker(),
    );
?>


<div class="container">
    <a class="navbar-brand" href="<?php echo home_url(); ?>">
        <?php hm_master_render_page_logo('page-logo', ''); ?>
    </a>
    <div class="d-flex flex-row">
        <div class="d-lg-none">
            <?php if ( function_exists( 'icl_get_languages' ) ): ?>
                <?php foreach ( $langs as $lang ) {
                    if ( ! $lang['active'] ) {
                        echo '<a class="lang-item btn btn-outline-light btn-sm lang-link text-body" href="' . $lang['url'] . '">' . $lang['native_name'] . '</a>';
                    }
                } ?>
            <?php endif; ?>
        </div>

        <button id="mobile-menu-toggle" class="navbar-toggler collapsed" type="button" data-toggle="modal" data-target="#mobile-nav">
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
    </div>

    <?php wp_nav_menu( $main_menu_args ); ?>
</div>
