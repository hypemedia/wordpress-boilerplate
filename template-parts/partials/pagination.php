<?php
	/**
	 * Pagination section for Blog pages
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */

	hm_master_print_pagination('#articles');
