<?php
	/**
	 * Card-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	// Base ACF Fields
	$image              = get_sub_field('image')            ?: array();
	$text               = get_sub_field('text')             ?: '';
	$background         = get_sub_field('background')       ?: 'bg-white';
	$textcolor          = get_sub_field('textcolor')        ?: 'text-body';
	$image_by_height    = get_sub_field('image_sizing')    ?: false;
	
	$link               = get_sub_field('link')             ?: array();
	$size               = get_sub_field('format')           ?: 'full';
	$img_as_bg          = get_sub_field('img_as_bg');
	$text_alignment     = get_sub_field('text_alignment')   ?: '';
	
	if($text_alignment) $text_alignment = " " . $text_alignment;
	
	// Initialize variables
	$link_html      = '';
	$card_html      = '';
	
	// Check link variable
	if(hm_master_check_item($link)) {
		$link_html = "<a href=\"" . $link['url'] . "\" class=\"stretched-link\" " . hm_master_get_link_target($link['target']) ."><span class=\"sr-only\">" . __('Seite ansehen', 'hm_master') . "</span></a>\n";
	}
	
	if(hm_master_check_item($image)) {
		if($img_as_bg) {
			$card_html .= "<div class=\"card border-0 card-body overlay overlay-" . $background . " " . $textcolor . " bg-full" . (hm_master_check_item($link) ? ' has-link' : '') . "\" style=\"background-image: url(" . $image['sizes']['medium_large'] . ")\">\n";
		} else {
			$card_html .= "<div class=\"card border-0" . (hm_master_check_item($link) ? ' has-link' : '') . "\">\n";
			
			// Check for image-size/-type
			if($size === 'auto') {
				$card_html .= wp_get_attachment_image($image['id'], 'medium_large', '', ['class' => ($image_by_height ? 'img-full-height' : 'img-full') . ' card-img m-auto']) . "\n";
			} else {
				if(!hm_master_check_item($size)) {
					$size = 'medium_large';
				}
				
				$size = ($size === 'full' ? $image['url'] : $image['sizes'][$size]);
				$card_html .= '<img class="img-full card-img m-auto" src="' . $size . '" alt="' . $image['alt'] . ' title="' . $image['title'] . '">';
			}
		}
		
		// If text append card-body and text-content
		if(hm_master_check_item($text)) {
		    if(!$img_as_bg) {
			    $card_html .= "<div class=\"card-body " . $background . " " . $textcolor . "\">\n";
            }
			$card_html .= "<div class=\"textbox d-flex h-100 flex-column" . $text_alignment . "\"><div class=\"d-flex h-100 flex-column" . $text_alignment . "\">" . $text . "</div></div>";
			// Close .card-body
			if(!$img_as_bg) {
				$card_html .= "</div>";
			}
		}
		
		$card_html .= $link_html;
		
		// Close .card
		$card_html .= "</div>";
	} else {
		$card_html .= "<div class=\"card border-0 card-body " . $background . " " . $textcolor . (hm_master_check_item($link) ? ' has-link' : '') . "\">\n";
		$card_html .= "<div class=\"textbox d-flex h-100 flex-column" . $text_alignment . "\"><div class=\"d-flex h-100 flex-column" . $text_alignment . "\">" . $text . "</div></div>";
		$card_html .= $link_html;
		$card_html .= "</div>";
	}
?>
<div class="card-widget card-<?php echo hm_master_check_item($image) ? 'image' : 'text'; ?>">
	<?php echo $card_html; ?>
</div>


