<?php
	/**
	 * Accordion-Widget.
	 *
	 * @package hm_master
	 * @since 1.2.2
	 * @author hype.media <web@hype-media.de>
	 */
	$id = "accordion-" . get_sub_field('akkordeon-id'); // String
	
?>
<div class="accordion-widget textbox h-100" id="<?php echo $id; ?>">
	<?php $pane_counter = 0;
		while ( have_rows( 'items' ) ): the_row();
			$title   = get_sub_field( 'title' );
			$content = get_sub_field( 'content' ); ?>
            <div class="accordion border-0">
                <div class="accordion-header<?php echo $pane_counter === 0 ? " active-item" : ""; ?>" id="<?php echo $id . "-heading-" . $pane_counter; ?>">
                    <h3 class="mb-0">
                        <button
                                class="btn btn-link btn-block text-left p-0<?php echo $pane_counter !== 0 ? " collapsed" : ""; ?>"
                                type="button"
                                data-toggle="collapse"
                                data-target="#<?php echo $id . "-collapse-" . $pane_counter; ?>"
                                aria-expanded="true"
                                aria-controls="#<?php echo $id . "-collapse-" . $pane_counter; ?>"
                        >
							<?php echo $title; ?>
                        </button>
                    </h3>
                </div>

                <div
                        id="<?php echo $id . "-collapse-" . $pane_counter; ?>"
                        class="collapse<?php echo $pane_counter === 0 ? " show" : ""; ?>"
                        aria-labelledby="<?php echo $id . "-heading-" . $pane_counter; ?>"
                        data-parent="#<?php echo $id; ?>"
                >
                    <div class="accordion-body">
						<?php echo $content; ?>
                    </div>
                </div>
            </div>
			<?php
			$pane_counter ++;
		endwhile;
	?>
</div>
