<?php
	/**
	 * Product-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	global $post;
	$pp = $post;
	
	$product_id = get_sub_field( 'product' );
	
	if ( $product_id ):
		$product_query = new WP_Query( array( 'post_type' => 'hm_master_products', 'p' => $product_id ) );
	
		echo "<div class=\"product-widget\">";
		
		if ( $product_query && $product_query->have_posts() ):
			while ( $product_query->have_posts() ): $product_query->the_post();
				get_template_part( '/template-parts/products/content' );
			endwhile;
			wp_reset_postdata();
			$post = $pp;
		endif;
		
		echo "</div>";
	endif;
