<?php
	/**
	 * Tabs-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>
<div class="tabs-widget textbox h-100">
    <nav>
        <div class="nav nav-tabs" role="tablist" aria-orientation="vertical">
			<?php $nav_counter = 0;
				while ( have_rows( 'tabs' ) ): the_row();
					$title = get_sub_field( 'title' ); ?>
                    <a class="nav-item nav-link text-body<?php echo $nav_counter === 0 ? " active" : ""; ?>" id="<?php echo hm_master_str_to_id( $title ); ?>" data-toggle="pill" href="#<?php echo hm_master_str_to_id( $title ); ?>-pane" role="tab" aria-controls="<?php echo hm_master_str_to_id( $title ); ?>-pane" aria-selected="<?php echo $nav_counter === 0 ? "true" : "false"; ?>">
                        <h5 class="mb-0 hyphenate"><?php echo $title; ?></h5>
                    </a>
					<?php $nav_counter ++;endwhile; ?>
        </div>
    </nav>
    <div class="tab-content h-md-100">
		<?php $pane_counter = 0;
			while ( have_rows( 'tabs' ) ): the_row();
				$title   = get_sub_field( 'title' );
				$content = get_sub_field( 'content' ); ?>
                <div class="tab-pane card-body border-top-0 bg-white fade<?php echo $pane_counter === 0 ? " show active" : ""; ?>" id="<?php echo hm_master_str_to_id( $title ); ?>-pane" role="tabpanel" aria-labelledby="<?php echo hm_master_str_to_id( $title ); ?>-tab">
					<?php echo $content; ?>
                </div>
				<?php
				$pane_counter ++;
			endwhile;
		?>
    </div>
</div>
