<?php
	/**
	 * Image-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$image      = get_sub_field('image');               // Array
	$size       = get_sub_field('size');                // String
	$link       = get_sub_field('link');                // Array
    
    // Check for image
    if(hm_master_check_item($image)) {
        $figure_html = '<figure class="image-widget-img' . (hm_master_check_item($link) ? ' w-link' : '') . '">';
	
	    // Check for image-size/-type
        if($size === 'auto') {
	        $figure_html .= wp_get_attachment_image($image['id'], 'medium_large', '', ['class' => 'img-full my-auto']);
        } else {
	        if(!hm_master_check_item($size)) {
		        $size = 'medium_large';
	        }
	        
	        $size = $size == 'full' ? $image['url'] : $image['sizes'][$size];
	        $figure_html .= '<img class="img-full my-auto" src="' . $size . '" alt="' . $image['alt'] . '" title="' . $image['title'] . '" alt="' . $image['alt'] . '">';
        }
        
	    $figure_html .= '</figure>';
    }
	
	// Check for link
	if(hm_master_check_item($link)) {
		$figure_html = '<a class="img-link" href="' . $link['url'] .'" ' . hm_master_link_target($link['target'], false) . '>' . $figure_html;
		$figure_html .= '</a>';
	}
?>
<!-- IMAGE WIDGET -->
<div class="image-widget mb-1">
	<?php echo $figure_html; ?>
</div>
<!-- /IMAGE WIDGET -->
