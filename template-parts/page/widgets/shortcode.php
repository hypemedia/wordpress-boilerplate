<?php
	/**
	 * Shortcode-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>
<div class="shortcode-widget textbox">
	<?php
        $shortcode = get_sub_field('shortcode', false);
		echo do_shortcode($shortcode);
    ?>
</div>
