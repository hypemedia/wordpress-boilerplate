<?php
	/**
	 * WYSWYG-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>
<div class="wysiwyg-widget textbox">
	<?php echo get_sub_field('editor'); ?>
</div>
