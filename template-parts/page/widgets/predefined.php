<?php
	/**
	 * Predefined-Widget.
	 *
	 * @package hm_master
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	global $post;
	$pp = $post;
	
	$object_id = get_sub_field( 'global-widget' );
	
	if ( $object_id ):
		$predefined_query = new WP_Query( array( 'p' => $object_id, 'post_type' => 'defined_content' ) );
		
		if ( $predefined_query && $predefined_query->have_posts() ):
			while ( $predefined_query->have_posts() ): $predefined_query->the_post();
				while ( have_rows( 'elements' ) ) : the_row();
					get_template_part( '/template-parts/page/widgets/' . get_row_layout() );
				endwhile;
			endwhile;
			wp_reset_postdata();
			$post = $pp;
		endif;
	endif;
