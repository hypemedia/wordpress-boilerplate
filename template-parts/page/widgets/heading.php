<?php
	/**
	 * Heading-Widget.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$level   = get_sub_field( 'level' );            // String
	$level_s = get_sub_field( 'level_style' );      // String
	$mb      = get_sub_field( 'margin_bottom' );    // String
	$heading = get_sub_field( 'heading' );          // String
	$weight  = get_sub_field( 'weight' );           // String
	$color   = get_sub_field( 'color' );            // String
	$link    = get_sub_field( 'link' );             // Array
	
	$start = '<' . $level . ' class="heading-widget ' . $level_s . ' ' . $mb . ' ' . $weight . ' ' . $color . '">';
	$end   = '</' . $level . '>';
	
	if ( hm_master_check_item( $link ) ) {
		echo "<a class=\"no-style heading-link\" href=\"" . $link['url'] . "\" " . hm_master_link_target( $link['target'] ) . ">";
	}
	
	echo $start . $heading . $end;
	
	if ( hm_master_check_item( $link ) ) {
		echo "</a>";
	}

