<?php
	/**
	 * Template for the Widget-Section.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	//** SETTINGS & ARGUMENTS **//
	
	// Output slider markup
	$generate_slider    = get_sub_field('as_slides'); // Bool
	$slick_options      = get_sub_field('slick_options'); // String
    $slide_class        = $generate_slider ? ' widget-slide-item' : '';
    
	// (Outer-) Padding-Settings
	$padding = get_sub_field('padding'); // Array
 
	// (Outer-) Column-Settings
    $column = get_sub_field('column'); // Array
    
    $column_class = hm_master_wysiwyg_column_switch(
            $column['column-orientation'],
            $column['column-width'],
            $column['column-breakpoint']
    );
    
    // HTML-ID Attribute
    $html_id = get_sub_field('attr_id'); // String
 
	// Background
	$background     = get_sub_field('background'); // Array
    $section_attrs  = hm_master_section_attrs($background, array('widget-section', hm_master_row_paddings($padding), ($generate_slider ? 'widget-slides' : '')), false); // String
?>
<section<?php echo $html_id ? ' id="' . $html_id . '" ' : ' '; ?><?php echo $section_attrs; ?>>
    <div class="section-inner <?php echo $column['container-width']; ?>">
        <div class="row <?php echo $column['container-width'] == 'container-full' ? 'no-gutters outer-row' : 'outer-row'; ?>">
            <div class="<?php echo $column_class; ?>">
                
                <?php if(have_rows('widgets')): ?>
                    <?php if($generate_slider): ?>
                        <!-- AS SLIDES -->
                        <div class="widget-slider-wrap">
                            <div class="widget-slider"<?php echo ($slick_options) ? " data-slick='" . $slick_options . "'" : ""; ?>>
                    <?php endif; ?>
                    
                    <!-- ROW -->
                    <div class="widgets row <?php echo $column['container-width'] == 'container-full' ? 'no-gutters inner-row' : 'inner-row'; ?><?php echo $slide_class; ?>">
                        
                        <?php while (have_rows('widgets')) : the_row();
	                        $col_width      = get_sub_field('width');           // String
	                        $col_width_mob  = get_sub_field('width_mobile');    // String
	                        $type           = get_sub_field('type');            // String
	                        $add_row        = get_sub_field('add_row');         // Bool
	                        $align          = get_sub_field('align');           // String
	                        $layout         = get_sub_field('layout');          // String
	                        $margin         = get_sub_field('margin');          // String
	                        $push           = get_sub_field('push');            // String
                            
                            $widget_classes = "widget";
                         
	                        if($col_width === 'auto') {
		                        $col_classes = 'widget-col col-' . $col_width_mob . ' col-md';
	                        } else {
		                        $col_classes = 'widget-col col-' . $col_width_mob . ' col-md-' . $col_width;
	                        }
	                        
	                        if($align === 'center') {
		                        $col_classes .= ' d-flex flex-column justify-content-center';
                            } elseif($align === 'strech') {
		                        $col_classes .= ' d-flex flex-column streched';
		                        $widget_classes .= " d-flex flex-column streched-widget";
                            }
		
		                    $col_classes .= ' mb-2';
	                        
	                        if(hm_master_check_item($margin)) {
		                        $col_classes .= ' ' . $margin;
                            }
	                        
	                        if(hm_master_check_item($push)) {
		                        $col_classes .= ' ' . $push;
                            } else {
		                        $col_classes .= " mx-0";
                            }
	                        
	                        if($type === 'article') {
	                            $element = 'article';
                            } else {
		                        $element = 'div';
                            }
	                        
	                        if($add_row) {
	                            echo "</div>\n<!-- /ROW - COMPUTED -->\n"; // Close previous .row
                                echo "<!-- ROW - COMPUTED -->\n<div class=\"widgets row " . ($column['container-width'] == 'container-full' ? 'no-gutters inner-row' : 'inner-row') . $slide_class . "\">";
                            }
                        ?>
                        <!-- WIDGET -->
                            <?php echo '<' . $element . ' class="'. $col_classes . '">'; ?>
                                <?php echo '<div class="' . $widget_classes . '">'; ?>
                                    <?php while (have_rows('elements')) : the_row();
                                        get_template_part('/template-parts/page/widgets/' . get_row_layout());
                                    endwhile; ?>
	                            <?php echo '</div>'; ?>
                            <?php echo '</' . $element . '>'; ?>
                        <!-- /WIDGET -->
                        <?php endwhile; ?>
                    </div>
                    <!-- /ROW -->
                    
	                <?php if($generate_slider): ?>
                            </div>
                            <div class="arrow-wrap d-flex flex-row">
                                <div class="arrows prev"><span class="sr-only"><?php _e('Zurück', 'hm_master'); ?></span></div>
                                <div class="arrows next"><span class="sr-only"><?php _e('Weiter', 'hm_master'); ?></span></div>
                            </div>
                        </div>
                        <!-- /AS SLIDES -->
	                <?php endif; ?>
	            <?php endif; ?>
             
            </div>
        </div>
    </div>
</section>
