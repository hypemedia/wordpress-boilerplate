<?php
	/**
	 * Template for the HTML-Section.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	// Args
    $html_id    = get_sub_field('attr_id'); // String
    $editor     = get_sub_field('editor', false);  // String
?>
<section<?php echo $html_id ? ' id="' . $html_id . '"' : ''; ?> class="html-section">
	<?php echo get_sub_field('editor', true); ?>
</section>
