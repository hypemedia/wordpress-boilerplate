<?php
	/**
	 * Template for the simple
	 * WYSWYG-Section.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	// Args
	$padding         = get_sub_field( 'padding' );              // Array
	$backgroundcolor = get_sub_field( 'backgroundcolor' );      // String
	$textcolor       = get_sub_field( 'textcolor' );            // String
	$column          = get_sub_field( 'column' );               // Array
	$column_class    = hm_master_wysiwyg_column_switch(
		$column['column-orientation'],
		$column['column-width'],
		$column['column-breakpoint']
	);                                                          // String
	$html_id         = get_sub_field( 'attr_id' );              // String
?>
<section<?php echo $html_id ? ' id="' . hm_master_str_to_id($html_id) . '"' : ''; ?> class="wysiwyg-section <?php echo hm_master_row_paddings($padding); ?> <?php echo $backgroundcolor; ?> <?php echo $textcolor ?>">
    <div class="section-inner <?php echo $column['container-width']; ?>">
        <div class="row <?php echo $column['container-width'] == 'container-full' ? 'no-gutters outer-row' : 'outer-row'; ?>">
            <div class="<?php echo $column_class; ?>">
                <div class="textbox">
                    <?php echo get_sub_field('editor'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
