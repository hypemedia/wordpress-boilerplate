<?php
	/**
	 * Fallback content for the page.php
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>


<?php if(have_posts()): while (have_posts()): the_post(); ?>
	<section class="page-section py-4">
		<div class="section-inner container">
			<div class="row">
				<div class="col-12">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>
<?php endwhile; endif; ?>
