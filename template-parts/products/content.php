<?php
	/**
	 * The products archive
	 * content file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$title_arrtibute = array(
		'before' => __( 'Zum Produkt: ', 'hm_master' ),
		'after'  => ''
	);
?>
<article id="post-<?php the_ID(); ?>" class="article-item text-center position-relative">
    <header class="article-header text-center">
        <figure class="article-thumbnail">
			<?php echo get_the_post_thumbnail( get_the_ID(), 'medium_large', [ 'class' => 'img-full my-auto' ] ); ?>
        </figure>
        <h2 class="h5 article-title t-t-n text-secondary">
			<?php the_title(); ?>
        </h2>
    </header>

    <div class="article-content">
		<?php echo hm_master_excerpt( 15 ); ?>
    </div>
    <a href="<?php echo esc_url( add_query_arg( 'return', urlencode( esc_url( hm_master_get_current_page_url() ) ), get_permalink() ) ); ?>" class="stretched-link" title="<?php the_title_attribute( $title_arrtibute ); ?>">
        <span class="sr-only"><?php _e( 'Produkt ansehen', 'hm_master' ) ?></span> </a>
</article>
