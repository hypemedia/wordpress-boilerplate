<?php
	/**
	 * The products archive
	 * content file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$title_arrtibute = array(
		'before' => __( 'Zum Produkt: ', 'hm_master' ),
		'after'  => ''
	);
?>
<article class="card-widget">
    <div id="post-<?php the_ID(); ?>" class="card border-0 has-link">
		<?php echo get_the_post_thumbnail( get_the_ID(), 'medium_large', [ 'class' => 'img-full card-img m-auto' ] ); ?>
        <div class="card-body text-center">
            <div class="textbox d-flex h-100 flex-column">
                <h3 class="h5 article-title">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute( $title_arrtibute ); ?>">
						<?php the_title(); ?></a>
                </h3>
                <div class="wrap my-auto">
					<?php echo hm_master_excerpt( 15 ); ?>
                </div>
            </div>
            <a href="<?php the_permalink(); ?>" class="stretched-link" title="<?php the_title_attribute( $title_arrtibute ); ?>">
                <span class="sr-only"><?php __( 'Produkt ansehen', 'hm_master' ) ?></span> </a>
        </div>
    </div>
</article>
