<?php
	/**
	 * The products single
	 * content file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$all_patient_info = array();
	$banner_image     = get_field( 'banner' ); // Array
 
	if ( hm_master_check_item($banner_image) ) {
		$background = array(
			'type'       => 'image',
			'bg_image'   => $banner_image,
			'bg_overlay' => 'overlay overlay-dark'
		);
	} else {
		$background = array(
			'type'       => 'color',
			'bg_color'   => 'bg-white',
			'bg_overlay' => 'overlay overlay-dark'
		);
	}
	
	
	// hero-section classes
	$classes = array(
		'image-area-section',
		'image-area-section-products',
		'background-full',
		'image-area-static',
		'intro'
	);
	
	$section_attrs = hm_master_section_attrs( $background, $classes, false, false, true );
	
	// Content
	$description = get_field( 'description' );
	
	// Patient Info
	$pat_info = get_field( 'pat-info' );
	
	// Return-Link
	if ( get_query_var( 'return' ) ) {
		$return_url            = urldecode( esc_url( get_query_var( 'return' ) ) );
		$back_to_products_text = __( 'Zurück zu den Ergebnissen', 'hm_master' );
	} else {
		$return_url            = get_post_type_archive_link( 'hm_master_products' );
		$back_to_products_text = __( 'Alle Produkte ansehen', 'hm_master' );
	}
	
	// Query Patient Information
	$ids_args = [
		'post_type'      => 'hm_master_patient_info',
		'posts_per_page' => - 1,
		'order'          => 'ASC',
		'fields'         => 'ids'
	];
	
	if ( hm_master_check_item( $pat_info ) ) {
		$all_patient_info = get_posts( $ids_args );
	}
?>

<!-- IMAGE-AREA-STATIC -->
<section <?php echo $section_attrs; ?>>
    <div class="section-inner container-full position-relative">
        <div class="image-track">
            <div class="image-item d-flex flex-column justify-content-center position-relative">
                <div class="image-item-inner">
                    <div class="container">
                        <div class="row no-gutters">
                            <div class="col-12 col-md-6">
                                <div class="content text-center text-md-left x-text-white products-single-description">
									<?php echo $description; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /IMAGE-AREA-STATIC -->

<!-- BREADCRUMBS -->
<section class="breadcrumb-section py-1 bg-white text-body">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav>
                    <a href="<?php echo $return_url; ?>" class="nav-link text-body p-0"><i class="fas fa-caret-left small"></i>&nbsp;<?php echo $back_to_products_text; ?>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</section><!-- /BREADCRUMBS -->

<?php
	if ( function_exists( 'get_field' ) && have_rows( 'flexflex' ) ):
		while ( have_rows( 'flexflex' ) ): the_row();
			get_template_part( '/template-parts/page/' . get_row_layout() );
		endwhile;
	endif;
?>

<?php
	if ( $all_patient_info ):
		
		$move_to_front = $pat_info;
		$post_ids_merged = array_merge( $move_to_front, $all_patient_info );
		$reordered_ids = array_unique( $post_ids_merged );
		
		$page_query = new WP_Query( array(
			'post_type'      => 'hm_master_patient_info',
			'post__in'       => $reordered_ids,
			'orderby'        => 'post__in',
			'order'          => 'ASC',
			'posts_per_page' => - 1,
		) );
		
		if ( $page_query && $page_query->have_posts() ):
			
			?>
            <section class="patient-info-section bg-white py-3">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="mb-3"><?php _e( 'Patienteninfos', 'hm_master' ); ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="lower slide-wrap">
                                <div class="patient-info-slider wide-slide row">
									<?php
										while ( $page_query->have_posts() ): $page_query->the_post();
											$opacity = '0.2';
											if ( $pat_info && hm_master_check_item( $pat_info ) ) {
												$opacity = ( in_array( get_the_ID(), $pat_info ) ? '1.0' : '0.2' );
											}
											
											echo "<article class=\"card-widget h-100 slide px-2\"><div id=\"post-" . get_the_ID() . "\" class=\"card shadow-none border-0 h-100 has-link\" style=\"opacity: " . $opacity . ";\">";
											get_template_part( '/template-parts/patient-info/content' );
											echo "</div></article>";
										endwhile;
										wp_reset_postdata();
									?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		<?php endif;endif; ?>

<!-- BREADCRUMBS -->
<section class="breadcrumb-section py-1 bg-secondary text-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav>
                    <a href="<?php echo get_post_type_archive_link( 'hm_master_products' ); ?>" class="nav-link text-white p-0"><i class="fas fa-caret-left small"></i>&nbsp;<span class="ml-auto"><?php _e( 'Alle Produkte ansehen', 'hm_master' ); ?></span>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</section><!-- /BREADCRUMBS -->
