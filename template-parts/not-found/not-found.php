<?php
	/**
	 * Template part for missing content
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>

<section class="content-not-found py-5">
    <div class="section-inner container text-center">
        <div class="row">
            <div class="col-12">
                <h1><?php esc_html_e( 'Nicht gefunden', 'hm_master' ); ?></h1>
                <p class="lead"><?php _e( 'Diese Seite konnte nicht gefunden werden.', 'hm_master' ); ?></p>
                <a href="<?php echo get_home_url(); ?>" class="btn btn-primary"><?php _e('Zurück zur Startseite'); ?></a>
            </div>
        </div>
    </div>
</section>
