<?php
	/**
	 * Template part for missing content
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
?>

<div class="col-12 col-sm-8 col-md-6 mx-auto">
	<div class="textbox">
		<?php get_template_part('template-parts/forms/search-form'); ?>
		<p class="mb-1"><?php printf( __( 'Es wurden keine mit Ihrer Suchanfrage <strong>%s</strong> übereinstimmenden Dokumente gefunden.', 'hm_master' ), get_search_query() ); ?></p>
		<ul class="list-clean">
			<li><?php _e('Vergewissern Sie sich, dass alle Wörter richtig geschrieben sind.', 'hm_master'); ?></li>
			<li><?php _e('Probieren Sie andere Suchbegriffe.', 'hm_master'); ?></li>
			<li><?php _e('Probieren Sie allgemeinere Suchbegriffe.', 'hm_master'); ?></li>
		</ul>
	</div>
</div>
