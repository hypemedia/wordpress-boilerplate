<?php
	/**
	 * Header Search-Form
	 * @package hm_master
	 * @version 1.0.1
	 * @since 1.2.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
?>

<form action="<?php echo $home_url; ?>" method="get">
    <div class="input-group input-group-lg search-field mb-3">
        <input type="text" name="s" id="search-field" class="form-control text-dark" placeholder="<?php _e('Suchen', 'hm_master'); ?>" value="<?php the_search_query(); ?>" aria-label="<?php _e( 'Suchen', 'hm_master' ); ?>" aria-describedby="header-search-form-icon">
        <div class="input-group-append">
            <button class="btn btn-primary" id="header-search-form-icon" type="submit">
                <i class="hm-icon-angle-right search-icon"></i>
                <span class="sr-only"><?php _e('Suchen', 'hm_master'); ?></span>
            </button>
        </div>
    </div>
</form>
