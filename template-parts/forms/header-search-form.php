<?php
	/**
	 * Header Search-Form
	 * @package hm_master
	 * @since 1.3.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
?>

<form action="<?php echo $home_url; ?>" method="get">
	<div class="input-group search-field">
		<input type="text" name="s" id="search-field" class="form-control" placeholder="<?php _e( 'Suchen', 'hm_master' ); ?>" value="<?php the_search_query(); ?>" aria-label="<?php _e( 'Suchen', 'hm_master' ); ?>" aria-describedby="header-search-form-icon">
		<div class="input-group-append border-bc bg-white">
			<button class="btn btn-link" id="header-search-form-icon" type="submit">
				<i class="hm-icon-angle-right search-icon" aria-hidden="true"></i>
				<span class="sr-only"><?php _e('Suchen', 'hm_master'); ?></span>
			</button>
		</div>
	</div>
</form>
