<?php
	/**
	 * The main template file
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = 'none';
	
	get_header();
?>

<main id="main-content" class="index-template">
	<?php if ( have_posts() ): ?>
        <section class="articles">
            <div class="section-inner container">
                <div class="row">
                    <div class="col-12">
						<?php while ( have_posts() ): the_post();
							get_template_part( '/template-parts/blog/content' );
						endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
	<?php endif; ?>
</main>

<?php get_footer(); ?>
