<?php
	/**
	 * The template for displaying 404 pages (not found)
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = 'none';
	
	get_header();
?>

<main id="main-content" class="404-template">
	<?php get_template_part( '/template-parts/not-found/not-found' ); ?>
</main>

<?php get_footer(); ?>
