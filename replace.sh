#!/bin/bash

if [ -n "$1" ]; then
  if [ -n "$2" ]; then
    echo "Replacing $1 with $2.";
    grep -ilr $1 * | xargs -I@ sed -i '' "s/$1/$2/g" @
  else
    echo "Second parameter not supplied. Choose a replacement for string $1."
  fi
else
  echo "First parameter not supplied. Usage: replace.sh old_string newstring"
fi
