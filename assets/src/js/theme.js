import 'bootstrap';

// Template
import { resetPage, themeInit } from "./theme/resetPage";
import { slideMobileMenu } from "./theme/mobileMenu";
import { sliderConfiguration } from "./theme/sliders";
//import { scrollHeader } from "./theme/scroll";
import { searchCollapse } from "./theme/collapse";
import { accordionWidget } from "./theme/accordion";

// jQuery wrapper to run on ready state
$(document).ready(() => {
    themeInit();
    resetPage();
    accordionWidget();
    //scrollHeader();
    slideMobileMenu();
    searchCollapse();
    sliderConfiguration();
});


// jQuery wrapper to run on resize
$(window).on('resize', () => {
    resetPage();
});
