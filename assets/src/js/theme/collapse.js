/**
 * Handling collapse!
 *
 * @package hm_master
 * @since 1.0.0
 * @author hype.media <web@hype-media.de>
 */

export function searchCollapse() {
    const searchBarCollapse = $('#search-bar');
    const searchBarAction = $('.search-action a');

    searchBarAction.on('click', (e) => {
        e.preventDefault();
        searchBarCollapse.collapse('toggle');
    });
}
