/**
 * This functions sets the initial
 * height to main to push the footer down
 * and sets the fixed header.
 *
 * @package hm_master
 * @since 1.0.0
 * @author hype.media <web@hype-media.de>
 */

export function resetPage() {
    // Elements
    const
        header      = document.getElementById('masthead'),
        footer      = document.getElementById('colophon'),
        main        = document.getElementById('main-content'),
        adminBar    = document.getElementById('wpadminbar');

    // Elements height
    let headerHeight    = header.offsetHeight;
    let footerHeight    = footer.offsetHeight;
    let windowHeight    = window.innerHeight;

    // Calculated heights
    let headfooHeight   = headerHeight + footerHeight;
    let mainHeight      = windowHeight - headfooHeight;

    // Adminbar case
    if(adminBar) {
        let adminHeight     = adminBar.offsetHeight;

        mainHeight      = mainHeight - adminHeight;
    }

    if(main) main.setAttribute('style', 'min-height: ' + mainHeight + 'px');
}

export function themeInit() {
    console.log("%cmade by hype.media siegen.", "color:#fff; font-family:sans-serif; font-size: 20px; padding: .25rem; background:#e10019;");
    $('.loading').fadeOut(400);
    $('[data-toggle="popover"]').popover({
        content: function () {
            return $(this).siblings('.highlight-content').html();
        },
        html: true,
        container: '#wrapper',
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header pb-0"></h3><div class="popover-body textbox"></div></div>',
        placement: function (context, source) {
            var position = $(source).position();

            console.log(position);

            if (position.left < 280) {
                return "top";
            }

            if (position.top < 280){
                return "bottom";
            }

            return "right";
        }
    });
}

export function showLoader() {
    $('.loading').fadeIn(400);
}

export function imageTooltips() {
    var highlights = document.getElementsByClassName('highlight-spot');

    // Track all tooltips trigger
    for (var i = 0; i < highlights.length; i++) {
        var hl_pos =  highlights[i].getAttribute("data-position");
        var pos = hl_pos.split('-');

        highlights[i].style.top = pos[0] + '%';
        highlights[i].style.left = pos[1] + '%';

        // // Event Handler
        // highlights[i].addEventListener("mouseenter", function(ev) {
        //     ev.preventDefault();
        //
        //     this.querySelector('.highlight').style.opacity = '1';
        //     this.querySelector('.highlight').style.visibility = 'visible';
        // });
        //
        // highlights[i].addEventListener("mouseleave", function(ev) {
        //     ev.preventDefault();
        //
        //     this.querySelector('.highlight').style.opacity = '0';
        //     this.querySelector('.highlight').style.visibility = 'hidden';
        // });
    }
}
