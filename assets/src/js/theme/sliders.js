/**
 * This file registers all the sliders.
 *
 * @package hm_master
 * @since 1.0.0
 * @author hype.media <web@hype-media.de>
 */
import 'slick-carousel';

export function sliderConfiguration() {
    const heroSlider = $('.hero-slide');
    const widgetSlider = $('.widget-slider');

    heroSlider.slick({
        dots: true,
        infinite: true,
        autoplay: true,
        speed: 800,
        rows: 0,
        autoplaySpeed: 4500,
        arrows: false,
        fade: true,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        adaptiveHeight: false,
    });

    widgetSlider.on('afterChange', function () {
    }).slick({
        dots: true,
        infinite: true,
        speed: 400,
        arrows: true,
        centerMode: false,
        fade: false,
        rows: 0,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    centerMode: false,
                    dots: true,
                    adaptiveHeight: true,
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 768,
                adaptiveHeight: true,
                settings: "unslick"
            }
        ]
    });
}
