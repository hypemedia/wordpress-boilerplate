/**
 * This functions help with handlig
 * the modal mobile menu.
 *
 * @package hm_master
 * @since 1.0.0
 * @author hype.media <web@hype-media.de>
 */
export function mobileMenu() {
    const hmm = $('#header-main-menu');
    const body = $('body');

    hmm.on('hidden.bs.collapse', function () {
        body.removeClass('open-menu');
    })

    hmm.on('shown.bs.collapse', function () {
        body.addClass('open-menu');
    })
}

export function slideMobileMenu() {
    let openmenu = false;

    $("#toggle-mobile-menu, .darkenarea").on('click', function (e) {
        const body = $("body");

        if( openmenu ) {
            openmenu = false;
            body.removeClass("openmenu");
        } else {
            openmenu = true;
            body.addClass("openmenu");
        }

    });

    $("#mobile-navigation .menu-item-has-children").each(function( index ) {
        let $this = $(this);
        const $maxh = ( $this.children(".sub-menu").height() + $this.height() ) * 1.7;

        $this.data("max-height", $maxh);
    });

    $("#mobile-navigation .menu > .menu-item-has-children > a").on('click', function (e) {
        let listitem = $(this).parent();

        if( !listitem.hasClass("open") ) {
            openItem( listitem, listitem.data("max-height") );
            e.preventDefault();
        }
    });

    $("#mobile-navigation .sub-menu > .menu-item-has-children > a").on('click', function (e) {
        let listitem = $(this).parent();

        if( !listitem.hasClass("open") ) {
            openSubItem( listitem, listitem.data("max-height") );
            e.preventDefault();
        }
    });

    $("#mobile-navigation .menu > .menu-item-has-children > i").on('click', function (e) {
        let listitem = $(this).parent();

        if( !listitem.hasClass("open") ) {
            openItem( listitem, listitem.data("max-height") );
        } else {
            $("#mobile-navigation .sub-menu > .menu-item-has-children").removeClass("open").css("max-height", "");
            listitem.removeClass("open").css("max-height", "");
        }
    });

    $("#mobile-navigation .sub-menu > .menu-item-has-children > i").on('click', function (e) {
        let listitem = $(this).parent();

        if( !listitem.hasClass("open") ) {
            openSubItem( listitem, listitem.data("max-height") );
        } else {
            $("#mobile-navigation .sub-menu > .menu-item-has-children").removeClass("open").css("max-height", "");
            listitem.removeClass("open").css("max-height", "");
        }
    });

    const openItem = function( listitem, maxh ) {
        $("#mobile-navigation .menu > .menu-item-has-children").removeClass("open").css("max-height", "");
        $("#mobile-navigation .sub-menu > .menu-item-has-children").removeClass("open").css("max-height", "");
        listitem.addClass("open").css("max-height", maxh);
    }

    const openSubItem = function( listitem, maxh ) {
        let subh = listitem.parent().parent().height();
        listitem.parent().parent().css("max-height", maxh + subh);
        $("#mobile-navigation .sub-menu > .menu-item-has-children").removeClass("open").css("max-height", "");
        listitem.addClass("open").css("max-height", maxh);
    }
}
