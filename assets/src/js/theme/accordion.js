/**
 * This functions help with handlig
 * accordions.
 *
 * @package hm_master
 * @since 1.0.0
 * @author hype.media <web@hype-media.de>
 */
export function accordionWidget() {
    const widgetCollapse = $('.accordion-widget .collapse');

    widgetCollapse.on('show.bs.collapse', function () {
        $(this).prev().addClass('active-item');
    });

    widgetCollapse.on('hide.bs.collapse', function () {
        $(this).prev().removeClass('active-item');
    });
}
