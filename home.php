<?php
	/**
	 * The blog template file
	 *
	 * @package hm_master
	 * @version 1.3.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$image_area             = 'none';
	$page_for_posts_query   = false;
	
	// Get the content from the pages
	$page_for_posts = get_option( 'page_for_posts' );
	
	// Get the content from page
	if ( $page_for_posts ) {
		$page_for_posts_query = new WP_Query( array( 'p' => $page_for_posts, 'post_type' => "page" ) );
	
        if( function_exists( 'get_field' ) ) {
		    $image_area = get_field( 'image_area', $page_for_posts ) ?: ''; // String
        }
	}
	
	$paged = get_query_var( 'paged', 1 );
	
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = $image_area;
	
	get_header();
?>

<main id="main-content" class="home-template">
	<?php
		// Loop for the home-template top-content
		if ( $page_for_posts_query && $page_for_posts_query->have_posts() ) {
			while ( $page_for_posts_query->have_posts() ) {
				// Setup the query
				$page_for_posts_query->the_post();
				
			    // Initialize variables
				$image_area = 'none';
				
				// Page Layout ...
				if( function_exists( 'get_field' ) ) {
					// Image-Area content
					$image_area = get_field( 'image_area' ); // String
     
					if ( $image_area !== 'none' ) {
						get_template_part( 'template-parts/partials/image-area-' . $image_area );
					}
				}
            }
			
            // Back to default query
			wp_reset_postdata();
        }
	?>
 
 
	<?php if ( have_posts() ): ?>
        <section class="articles bg-white py-5">
            <div class="section-inner container">
                <div class="row">
                        <?php
                            // Loop through the posts and display them accordingly
                            while ( have_posts() ) {
                                the_post();
                                echo "<div class=\"col-12 col-md-6 col-lg-4 d-flex flex-column justify-content-center streched\">";
                                get_template_part( '/template-parts/blog/content' );
                                echo "</div>";
                            }
                        ?>
                </div>
            </div>
        </section>
	<?php endif; ?>
	
    <?php if(hm_master_show_bootstrap_pagination()): ?>
        <section class="pagination py-3 bg-bc">
            <div class="section-inner container">
                <div class="row">
                    <div class="col-12">
                        <?php hm_master_print_pagination(); ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
	
    <?php
		// Loop for the home-template top-content
	    if ( $page_for_posts_query && $page_for_posts_query->have_posts() ) {
		    while ( $page_for_posts_query->have_posts() ) {
			    // Setup the query
			    $page_for_posts_query->the_post();
			
			    // Page Layout ...
			    if( function_exists( 'get_field' ) ) {
				    if ( function_exists( 'get_field' ) && have_rows( 'flexflex' ) ) {
					    while ( have_rows( 'flexflex' ) ) {
						    the_row();
						    get_template_part( '/template-parts/page/' . get_row_layout() );
					    }
				    }
			    } else {
				    // ... or just get the default content
				    get_template_part( '/template-parts/page/page' );
			    }
            }
		
		    // Back to default query
		    wp_reset_postdata();
        }
	?>
</main>

<?php get_footer(); ?>
