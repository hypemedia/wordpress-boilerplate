<?php
	/**
	 * The archives template file
	 *
	 * @package hm_master
	 * @version 1.3.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	// Get the current taxonomy term
	$term = get_queried_object();
	
	$image_area = 'none';
	$header     = '';
	
	if( function_exists( 'get_field' ) ) {
		$image_area     = get_field( 'image_area', $term ) ?: ''; // String
		$header         = get_field( 'header', $term ) ?: ''; // String
    }
	
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = $image_area;
	
	get_header($header);
?>

<main id="main-content" class="home-template">
	<?php
		// Loop for the page-content
        if( function_exists( 'get_field' ) ) {
	        // Image-Area content
	        if ( $image_area !== 'none' ) {
		        get_template_part( 'template-parts/partials/image-area-' . $image_area );
	        }
	
	        // Page Layout
	        if ( have_rows( 'flexflex', $term ) ) {
		        while ( have_rows( 'flexflex', $term ) ) {
			        the_row();
			        get_template_part( '/template-parts/page/' . get_row_layout() );
		        }
	        }
        }
	?>
 
	<?php if ( have_posts() ): ?>
        <section class="articles bg-white py-5">
            <div class="section-inner container">
                <div class="row">
                    <?php
                        while ( have_posts() ) {
                            the_post();
	                        echo "<div class=\"col-12 col-md-6 col-lg-4 d-flex flex-column justify-content-center streched\">";
                            get_template_part( '/template-parts/blog/content' );
                            echo "</div>";
                        }
                    ?>
                </div>
            </div>
        </section>
	<?php endif; ?>
	
	<?php if(hm_master_show_bootstrap_pagination()): ?>
        <section class="pagination py-3 bg-bc">
            <div class="section-inner container">
                <div class="row">
                    <div class="col-12">
						<?php hm_master_print_pagination(); ?>
                    </div>
                </div>
            </div>
        </section>
	<?php endif; ?>
</main>

<?php get_footer(); ?>
