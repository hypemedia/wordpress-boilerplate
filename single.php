<?php
	/**
	 * The template for displaying all single posts
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$image_area = 'none';
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = 'none';
	get_header();
?>

<main id="main-content" class="single-posts-template">
    <?php while ( have_posts() ): the_post();
	    get_template_part( '/template-parts/blog/content', 'single' );
	endwhile; ?>
</main>

<?php get_footer(); ?>
