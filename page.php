<?php
	/**
	 * The main file for static pages
	 *
	 * @package hm_master
	 * @version 1.1.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$image_area = 'none';
	
	if ( function_exists( 'get_field' ) ) {
	    $image_area = get_field( 'image_area' ); // String
    }
	
	$GLOBALS['HM_MASTER']['IMAGE_AREA'] = $image_area;
	
	get_header();
?>

<main id="main-content" class="pages-template">
	<?php
		// Image area
		if ( function_exists( 'get_field' ) && $image_area !== 'none' ) {
			get_template_part( 'template-parts/partials/image-area-' . $image_area );
		}
		
		// Loop through flexible field
		if ( function_exists( 'get_field' ) && have_rows( 'flexflex' ) ):
			while ( have_rows( 'flexflex' ) ): the_row();
				get_template_part( '/template-parts/page/' . get_row_layout() );
			endwhile;
		else :
			get_template_part( '/template-parts/page/page' );
		endif;
	?>
</main>

<?php get_template_part( '/template-parts/partials/popups' ); ?>

<?php get_footer(); ?>
