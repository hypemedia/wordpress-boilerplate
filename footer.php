<?php
	/**
	 * The template for displaying the footer
	 *
	 * @package hm_master
	 * @version 1.0.2
	 * @since 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	
	$footer_menu_args = array(
		'theme_location'    => 'footer',
		'depth'             => 1,
		'container'       => 'div',
		'container_class' => 'footer-menu mt-auto',
		'menu_class'      => 'nav justify-content-end',
		'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
		'walker'          => new WP_Bootstrap_Navwalker(),
	);
	
	$mobile_menu_args = array(
		'theme_location' => 'main',
		'container'		 => false,
		'after'			 => '<i></i>',
		'fallback_cb'    => false
	);
	
	$bottom_col_1 = get_theme_mod( 'hm_master_theme_options_footer_column_1' );
	$bottom_col_2 = get_theme_mod( 'hm_master_theme_options_footer_column_2' );

?>

<footer id="colophon">
	<?php if(
		is_active_sidebar('footer-column-1') ||
		is_active_sidebar('footer-column-2') ||
		is_active_sidebar('footer-column-3') ||
		is_active_sidebar('footer-column-4')):
		?>
        <div class="footer-inner footer-top bg-primary text-white py-4">
            <div class="container">
                <div class="row">
					<?php if( is_active_sidebar('footer-column-1') ): ?>
                        <div class="col-12 col-md-6 col-lg-4 mb-2 mb-lg-0">
							<?php dynamic_sidebar('footer-column-1'); ?>
                        </div>
					<?php endif; ?>
					<?php if( is_active_sidebar('footer-column-2') ): ?>
                        <div class="col-12 col-md-6 col-lg mb-2 mb-lg-0">
							<?php dynamic_sidebar('footer-column-2'); ?>
                        </div>
					<?php endif; ?>
					<?php if( is_active_sidebar('footer-column-3') ): ?>
                        <div class="col-12 col-md-6 col-lg mb-2 mb-lg-0">
							<?php dynamic_sidebar('footer-column-3'); ?>
                        </div>
					<?php endif; ?>
					<?php if( is_active_sidebar('footer-column-4') ): ?>
                        <div class="col-12 col-md-6 col-lg mb-lg-0">
							<?php dynamic_sidebar('footer-column-4'); ?>
                        </div>
					<?php endif; ?>
                </div>
            </div>
        </div>
	<?php endif; ?>
    <div class="footer-inner footer-bottom py-3 bg-white">
        <div class="container">
            <div class="row">
				<?php if( $bottom_col_1 ): ?>
                    <div class="col-12 col-md-4 mb-2 mb-lg-0 d-flex flex-column">
                        <div class="textbox my-auto text-center text-lg-left">
							<?php echo $bottom_col_1; ?>
                        </div>
                    </div>
				<?php endif; ?>
				<?php if( $bottom_col_2 ): ?>
                    <div class="col-12 col-md-4 mb-2 mb-lg-0 d-flex flex-column">
                        <div class="textbox my-auto text-center text-lg-left">
							<?php echo $bottom_col_2; ?>
                        </div>
                    </div>
				<?php endif; ?>
                <div class="col-12 col-lg-4 d-flex justify-content-center justify-content-lg-end">
					<?php wp_nav_menu( $footer_menu_args ); ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="darkenarea"></div>
<aside id="mobile-navigation">
    <header class="bg-white d-flex flex-row">
        <button class="navbar-toggler ml-auto" id="toggle-mobile-menu">
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
    </header>
	<?php wp_nav_menu( $mobile_menu_args ); ?>
</aside><!-- #mobile-navigation -- -->

</div> <!-- #wrapper -->

<?php wp_footer(); ?>
<noscript>.loading, .card-slider, .content-slide .image-item {display: none !important;} .collapse, .tab-pane, .content-slide, .content-slide .image-item:first-child{display: block!important;opacity: 1!important;visibility: visible!important;}.image-area-section .image-item-inner {top: 0!important;}</style></noscript>
</body>
</html>
