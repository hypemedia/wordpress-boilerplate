# hype.media WordPress Master

hype.media boilerplate theme for WordPress projects without Gutenberg.

## Things you need to know

This theme is made for [Wordpress docker-compose Starterkit](https://github.com/juliantroeps/wordpress-docker-compose) workflow. The assets are processed with the Webpack-Setup in the repository.
If you don't want to use the Sass setup you can remove the `main.css` file from the `wp_enqueue_scripts` in the `functions.php` and add the WordPress stylesheet there.

Inside the `functions.php` are all the crutial WordPress theme setup functions and actions. Additional functions go inside the `admin-functions.php` and `theme-functions.php`.

We use [Advanced Custom Fields Pro](https://www.advancedcustomfields.com/) to build our page content. The main ACF-Field is the [Flexible Content Field](https://www.advancedcustomfields.com/resources/flexible-content/).
Inside there are our subfields. Each subfield is placed in its own file inside `/template-parts/page/{fieldtitle}.php`. More information at the [ACF Docs](https://www.advancedcustomfields.com/resources/flexible-content/).

For theme styles we use the latest [Bootstrap source](https://getbootstrap.com/docs/4.5/getting-started/download/#source-files). Remove unneeded `@imports` from the root `_bootstrap.scss` and edit the `_variables.scss` to update the styles. Add addtional styles inside the `main.scss` or create new files.

Always use the latest WordPress version.

## Getting Started

*Search and replace for `hm_master` and add your own text-domain and function prefix. Run `sh replace.sh hm_master YOUR_STRING` in the root of the theme to replace the function-prefix and delete the old `.git` as well as the `replace.sh` file (can be a vulnerability).* You can find Docker and webpack instructions [here](https://github.com/juliantroeps/wordpress-docker-compose).
Update the theme name in the `screenshot.psd`, export it again and delete the PSD-file. Update the LICENSE if needed or remove it to make it exclusive.

Bootstrap, Popper.js and jQuery via `node_modules` are required. Slick (`npm install --save slick-carousel`) is optional. 

Run `npm install --save bootstrap popper.js jquery` to install those dependencies.

At [/wp-admin/edit.php?post_type=acf-field-group&page=acf-tools]() you can import each ACF-JSON file from `inc/acf-json/` and start editing with our base fields.

## Additional Documentaion

- [WordPress](https://developer.wordpress.org/reference/), [Template Hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/)
- [Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/), [Bootstrap Theeming](https://getbootstrap.com/docs/4.4/getting-started/theming/#sass)
- [Advanced Custom Fields](https://www.advancedcustomfields.com/resources/)
- [docker-compose Starterkit](https://github.com/juliantroeps/wordpress-docker-compose)
- [Sass](https://sass-lang.com/guide)

## Changelog

### 1.3.2 (2021-06-29)

- Updates from latest projects

### 1.3.[0, 1] (2021-04-13)

- Updates from latest projects

### 1.2.2 (2020-07-15)

- Added accordion widget
- Update main Sass ans JavaScrip files (app.scss => theme.scss | app.js => theme.js) since "app" should be reserved for actual apps.
- Updated README.md
- Some smaller updates and refactoring

### 1.2.1 (2020-07-14)

- Updated prefix on post type pages
- Updated README.md

### 1.2.0 (2020-05-08)

- Updated Page-Builder
- Added Widgets (inc/widgets.php)
- Remove comments (inc/disbale-comments.php)
- Added gallay Overwrite (inc/gallery.php)
- Added image and attachment redirects (image.php | attachement.php)
- Added optional custom post types
- Lot of smaller updates

### 1.1.0 (2019-06-03)

- Updated Screenshot
- Updated Cookie Consent
- Updated Assets structure

### 1.0.3 (2019-06-03)

- Removed a function add twice
- Add string replacement script

### 1.0.2 (2018-10-11)

  * updated functions.php, style.css

### 1.0.1 (2018-10-10)

  * added editor-styles, bootstrap pagination function, helpers
  * updated bootstrap styles
  * renamed template-template.php to template-functions.php

### 1.0.0 (2018-10-10)

  * Initial commit
