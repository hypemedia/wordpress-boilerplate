<?php
	/**
	 * Theme functions and definitions
	 *
	 * Use prefix hm_master_ (text-domain) for
	 * functions to avoid conflicts.
	 *
	 * @package hm_master
	 * @version 1.0.0
	 * @author hype.media <web@hype-media.de>
	 */
	$GLOBALS['HM_MASTER'] = array();

	if ( ! function_exists( 'hm_master_theme_setup' ) ) {
		function hm_master_theme_setup() {

			// Make theme available for translation.
			load_theme_textdomain( 'hm_master', get_template_directory() . '/languages' );

			// Theme supports automatic title tags
			add_theme_support( 'title-tag' );

			// Theme supports post-thumbnails
			add_theme_support( 'post-thumbnails' );

			// Theme supports custom logo
			add_theme_support( 'custom-logo' );

			// Theme supports custom editor styles
			add_theme_support( 'editor-styles' );
			
			// WooCommerce
			add_theme_support( 'woocommerce', array(
				'single_image_width' => 600,
			) );
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );
			
			// Thumbnail Sizes Hero
			add_image_size( 'hero_xl', 1920, 800, true );
			add_image_size( 'hero_lg', 1800, 600, true );
			add_image_size( 'hero_sm', 1200, 400, true );

			// Thumbnail Sizes Card
			add_image_size( 'card_lg', 1600, 1067, true );
			add_image_size( 'card_md', 1200, 800, true );
			add_image_size( 'card_sm', 600, 400, true );

			// Taller Cards
			add_image_size( 'nsq_lg', 1600, 1333, true );
			add_image_size( 'nsq_md', 1200, 1000, true );
			add_image_size( 'nsq_sm', 600, 500, true );


			// Remove unnecessary wp_head links
			remove_action( 'wp_head', 'wp_generator' );
			remove_action( 'wp_head', 'wp_shortlink_wp_head' );
			remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
			remove_action( 'wp_head', 'feed_links_extra', 3 );

			// Register wordpress navs
			register_nav_menus( array(
				'main'   => esc_html__( 'Hauptmenü', 'hm_master' ),
				'mobile' => esc_html__( 'Hauptmenü (Mobile)', 'hm_master' ),
				'footer' => esc_html__( 'Footermenü', 'hm_master' ),
			) );

			// Make theme HTML5 ready
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

		}
	}
	add_action( 'after_setup_theme', 'hm_master_theme_setup' );

	/**
	 * Enqueue scripts and styles.
	 *
	 */
	function hm_master_scripts() {
		wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/assets/dist/css/theme.css', array(), '1618308100', '' );
		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/dist/js/theme.js', array(), '1618308100', true );
		# wp_enqueue_script( 'hmc', get_template_directory_uri() . '/assets/vendor/hmc.js', array(), '', true );
	}

	add_action( 'wp_enqueue_scripts', 'hm_master_scripts' );

	/**
	 * Save ACF fields to local JSON files
	 *
	 */
	add_filter( 'acf/settings/save_json', 'hm_master_acf_json_save_point' );
	function hm_master_acf_json_save_point( $path ) {
		$path = get_stylesheet_directory() . '/inc/acf-json';

		return $path;
	}

	/**
	 * Load ACF fields from local JSON files
	 *
	 */
	add_filter( 'acf/settings/load_json', 'hm_master_acf_json_load_point' );
	function hm_master_acf_json_load_point( $paths ) {
		unset( $paths[0] );
		$paths[] = get_stylesheet_directory() . '/inc/acf-json';

		return $paths;
	}

	/**
	 * Remove basic WP meta boxes to speed backend up
	 *
	 */
	add_filter( 'acf/settings/remove_wp_meta_box', '__return_true' );

	/**
	 * Modify url base from wp-json to 'api'
	 *
	 * @param $slug
	 *
	 * @return string
	 */
	function hm_master_api_slug( $slug ) {
		return 'api';
	}
	add_filter( 'rest_url_prefix', 'hm_master_api_slug' );

	/**
	 * Update the search url
	 */
	function hm_master_change_search_url() {
		if ( is_search() && ! empty( $_GET['s'] ) ) {
			wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
			exit();
		}
	}
	add_action( 'template_redirect', 'hm_master_change_search_url' );

	/**
	 * Additional functions and definitionen are
	 * placed in extra files inside ./inc/ directory.
	 *
	 */
	require get_template_directory() . "/inc/admin-functions.php";
	require get_template_directory() . "/inc/template-functions.php";
	require get_template_directory() . "/inc/template-pagination.php";

	// Custom Post Types
	if( function_exists( 'get_field' ) ) require get_template_directory() . "/inc/post-types/predefined-content.php";

	// Optional
	require get_template_directory() . "/inc/disable-comments.php";
	require get_template_directory() . "/inc/disable-wp-emoji.php";
	require get_template_directory() . "/inc/gallery.php";
	require get_template_directory() . "/inc/widgets.php";
	
	// Customizer
	require get_template_directory() . "/inc/customizer.php";
	
	// Nav Walker
	require get_template_directory() . "/inc/walker/ClassBootstrapNavWalker.php";
